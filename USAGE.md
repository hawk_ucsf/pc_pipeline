# Prostate Processing Workflow #

## Getting the Data ##

Before processing can begin, you must first pull over the images and raw spectra, and assign a PC number. This is accomplished with the command `pc_get_data`. This command, when given an accession number, will

1. Search for the relevant images and spectra
2. Validate that data required for processing is present
3. Automatically assign a PC number, based on exams in this current directory
4. Create a PC directory, and copy over the exam data for processing

### `pc_get_data` Usage###

1. Change into `/data/pca1`:

        cd /data/pca1

2. Run `pc_get_data`, with the exam's accession number:

        pc_get_data -a 12345678

3. You will see the following output, describing what exam data was found:

        Searching for exams...
        Parsing exams...
        Exams found:
            /data/dicom_cb/UCSF-MBMR1/11111111/E1545
        Raw spectra found:
            /data/raw_parn/UCSF-MBMR1/P25600.7.00_10141553
            /data/raw_parn/UCSF-MBMR1/P26112.7.00_10141553
            /data/raw_parn/UCSF-MBMR1/P26624.7_10141601
            /data/raw_parn/UCSF-MBMR1/ideal_fldata_raw1545_11_1.dat_10141611

4. The PC directory will then be created in your current directory (in this case, `/data/pca1`). The PC number will be automatically assigned, based on the highest numbered PC directory found. You will then see the data transfer progress:

        Copying /data/dicom_cb/UCSF-MBMR1/11111111/E1545 to /data/pca1/pc1234...
        ( 1 of 27) Copying Series 1...
        ( 2 of 27) Copying Series 2...
        ( 3 of 27) Copying Series 3...
        ...

5. When the data has finished copying, you will see a summary of what was copied over:

        =========================
        Exams transferred from /data/dicom_cb/UCSF-MBMR1/11111111/E1545 to /data/pca1/pc1234:
        =========================
            - /data/dicom_cb/UCSF-MBMR1/11111111/E1545
        =========================
        Raw files transferred from /data/dicom_cb/UCSF-MBMR1/11111111/E1545 to /data/pca1/pc1234:
        =========================
            - /data/raw_parn/UCSF-MBMR1/P25600.7.00_10141553
            - /data/raw_parn/UCSF-MBMR1/P26112.7.00_10141553
            - /data/raw_parn/UCSF-MBMR1/P26624.7_10141601
            - /data/raw_parn/UCSF-MBMR1/ideal_fldata_raw1545_11_1.dat_10141611
        =========================
        Review images in /data/pca1/pc1234/review_images:
        =========================
            - pc1234_S300_Ax_T1
            - pc1234_S400_Ax_T2
            - pc1234_S500_Ax_Cube_T2
            - pc1234_S504_Sag_Reformat
            - pc1234_S505_Cor_Reformat

6. Finally, the DCE and preGd combined coil images will be created, and put in the `DCE_combined_DICOM/` directory. The T5 DCE and anatomic images will be converted to `.idf`, and placed in `ReviewImages/`. You will see the following output:

        Creating combined coil images...
            Created: /data/pca1/pc1234/DCE_combined_DICOM/1009
            Created: /data/pca1/pc1234/DCE_combined_DICOM/1010
            Created: /data/pca1/pc1234/DCE_combined_DICOM/1009_t5
            Created: /data/pca1/pc1234/DCE_combined_DICOM/1010_t5
            Created: /data/pca1/pc1234/DCE_combined_DICOM/1010_t20
        Converting review_images/ dicoms to .idfs in ReviewImages/
            Converting /data/pca1/pc1234/review_images/pc1234_S505_Cor_Reformat
            Converting /data/pca1/pc1234/review_images/pc1234_S300_Ax_T1
            Converting /data/pca1/pc1234/review_images/dce_t5
            Converting /data/pca1/pc1234/review_images/pc1234_S504_Sag_Reformat
            Converting /data/pca1/pc1234/review_images/pc1234_S500_Ax_Cube_T2
            Converting /data/pca1/pc1234/review_images/pc1234_S400_Ax_T2

### `pc_get_data` Options ###
- If some data is missing or wasn't acquired, but you still want to copy the exam over and start processing, you can ignore validation failures with the `force` option:

        pc_get_data -a 12345678 -f

    or

        pc_get_data -a 12345678 --force
 
- You can assign a PC number, instead of having it auto-assigned, by passing the `--pc_num` flag:

        pc_get_data -a 12345678 --pc_num pc1234

- You can search for exam data, and have the results displayed, without copying over the data, by passing `-d` or `--dump`:

        pc_get_data -a 12345678 --dump

    The information displayed will be a little more verbose:

        Searching for exams...
        Parsing exams...
        Exams found:
          Exam :   /Path :  /data/dicom_cb/UCSF-MBMR1/11111111/E1545
                   Accession :  12345678
                   Date :  2015-10-14
                   Description :  MR PROSTATE WITH AND WITHOUT CONTRAST
                   Exam :  1545
                   Modality :  MR
                   Patient :   MRN :  11111111   Name :  DOE JOHN
                   Protocol :  Prostate_8_3_2015
                   Station :   Field :  3T
                               Make :  GE MEDICAL SYSTEMS
                               Model :  DISCOVERY MR750
                               Name :  UCSF-MBMR1
                   Time :  15:16:57
          Series : [ 1:   /Path :  /data/dicom_cb/UCSF-MBMR1/11111111/E1545/1
                          Description :  3 plane Loc FB
                          Images : 29
                          Transferred : True
                     2:   /Path :  /data/dicom_cb/UCSF-MBMR1/11111111/E1545/2
                          Description :  LOC
                          Images : 12
                          Transferred : True
                     3:   /Path :  /data/dicom_cb/UCSF-MBMR1/11111111/E1545/3
                          Description :  AX T1 3D
                          Images : 68
                          Transferred : True
                     ...]
        Raw spectra found:
            /data/raw_parn/UCSF-MBMR1/P25600.7.00_10141553
            /data/raw_parn/UCSF-MBMR1/P26112.7.00_10141553
            /data/raw_parn/UCSF-MBMR1/P26624.7_10141601

## Running the Processing Pipeline ##

The processing pipeline is controlled by `pc_run_pipe`. When run in a PC directory, created by `pc_get_data`, it will run through:

1. DCE processing
2. ADC map generation
3. Additional map generation (T1, ductal, cancer, high grade)
4. MRSI processing

Before each step, data validation occurs, to make sure that the needed files are present.

### `pc_run_pipe` Usage ###

1. Change into the PC directory, i.e. `/data/pca1/pc1234`:

        cd /data/pca1/pc1234

2. Run `pc_run_pipe`:

        pc_run_pipe

    T2 and DCE centerpoints can be passed to the pipeline, using the `-c` flag, to apply a shift. 6 values must be given (`x y z` coordinates for the T2 and DCE, respectively):

            pc_run_pipe -c 1.0 0 0 0.5 0 0

    You will then be asked to confirm:

            T2 Center: [1.0, 0.0, 0.0]
            DCE Center: [0.5, 0.0, 0.0]
            LPS Shift: [0.5, 0.0, 0.0],
            Proceed with shift ([y]/n)?

As the pipeline progresses through each step, you will see output like the following:

    Validating...
    Running DCE workflow...
        Generating DCE maps...
        Normalizing DCE series...
        Downsampling DCE series...

    Validating...
    Processing High-B...
        Generating Avg map...
    Processing rFOV...
        Generating Geo Mean map...
        Generating ADC map...
    ...

If one of the steps is missing some required data, you will see a ValidationFail:

    ValidationFail:   
      Raw Failures : []
      Series Failures : [ * DCE series is incomplete/missing.
                          * preGd series is incomplete/missing. ]

Individual steps can be run, either to repeat the step and create new results, or if there was a failure in the pipeline and you want to run a step that wasn't completed.

### DCE Processing ### 

`pc_run_dce` controls the DCE steps. When run in a PC directory, created by `pc_get_data`, it will:

1. Pull out the combined coil images, if not already in `DCE_combined_DICOM/`
2. Shift the DCE series, if centerpoints are provided, writing to `perfusion/shifted_dce`
3. Generate baseline, peak height, peak time, slope, and washout maps, writing to `perfusion/dcm_output`
4. Normalize the DCE series, writing to `perfusion/normalized_dce`
5. Downsample the DCE series, writing to `perfusion/dynamic_data_downsampled`

Example usage:

- `pc_run_dce`
- `pc_run_dce -c 1.0 0 0 0.5 0 0`

Each step can be run individually:

- `pc_run_dce --combo_coil` will create combined coil images
- `pc_run_dce --maps` will generate the maps
- `pc_run_dce --curves` will normalize and downsample
- `pc_run_dce --maps_and_curves` will run through both map fitting and curve steps

### ADC Processing ###

`pc_run_adc` controls the ADC steps. When run in a PC directory, created by `pc_get_data`, it will look for the different types of DWI series acquired, and:

1. Generate an average map for a coil-corrected High-B or High-B rFOV
2. Generate geometric mean and ADC maps for a rFOV or rFOV Brachy
3. Run `diffu_dicom` for a Standard Diffusion or Standard Diffusion Brachy

Results are written to `tensor/`.

Example usage:

- `pc_run_adc`

Each step can be run individually:

- `pc_run_adc --high_b`
- `pc_run_adc --high_b_rfov`
- `pc_run_adc --rfov`
- `pc_run_adc --rfov_brachy`
- `pc_run_adc --standard_diff`
- `pc_run_adc --standard_diff_brachy`

### Additional Map Generation ###

`pc_run_maps` will generate the extra maps. When run in a PC directory, created by `pc_get_data`, it will:

1. Generate a T1 map, writing to `t1/`
2. Generate a Ductal map, writing to `ductal/`
3. Generate Cancer probability and High Grade maps, writing to `cancer_maps/`

Example usage:

- `pc_run_maps`

Each step can be run individually:

- `pc_run_maps --t1`
- `pc_run_maps --ductal`
- `pc_run_maps --cancer`
- `pc_run_maps --high`

### MRSI Processing ###

`pc_run_mrsi` processes the raw spectroscopy data. When run in a PC directory, created by `pc_get_data`, it will:

1. Generate `.ddf` and `.cmplx` files from the pfile with `svk_gepfile_anon` and `svk_gepfile_reader`
2. Reconstruct with `set_fb_pars_prostate` and `process_spec_v6` (using `prostate_epsi_phase_correct_hack`, to account for the epsi flyback acquisition)
3. Quantify and phase using `run_csi_image_epsi`, `autophase`, and `process_p`

Results are written to `spectra/`.

Example usage:

- `pc_run_mrsi`

## Sending Results to PACS and Dynacad ##

After pipeline results are generated, and DCE/Spectroscopy screenshots have been made with `pc_sivic`, reidentified dicoms are generated and sent to PACS/Dynacad with `pc_send_to_pacs`. When run in a PC directory, it will:

1. Create dicoms with appropriate identifiers and UID's in `PACS_Export/`
2. Export to PACS
3. Export relevant series to Dynacad

Results can be generated, but not sent anywhere, using `--no_send`:

    pc_send_to_pacs --no_send

Conversely, generated results in `PACS_Export/` can be sent or resent, using `--re_send`

    pc_send_to_pacs --re_send

Series that are Sent to Pacs:

- Combined coil DCE and preGd
- Perfusion peak, slope, and washout maps
- ADC and average diffusion maps
- Sivic DCE curve and spectroscopy screenshots

Series that are Sent to Dynacad:

- Combined coil DCE and preGd
- Perfusion peak, slope, and washout maps
- ADC and average diffusion maps
- Axial T2 images

Dicoms in `PACS_Export/` will not be overwritten, meaning results by default can only be generated once. This is to prevent duplicate data, with different UIDs, being sent to PACS.

Individual results can be generated and sent:

    pc_send_to_pacs --gen_combo       
    pc_send_to_pacs --gen_perfusion   
    pc_send_to_pacs --gen_diffusion   
    pc_send_to_pacs --gen_sivic
    pc_send_to_pacs --gen_spectra
    pc_send_to_pacs --gen_dce_l
    pc_send_to_pacs --gen_dce_r
    pc_send_to_pacs --gen_dce_all       
    pc_send_to_pacs --send_combo      
    pc_send_to_pacs --send_perfusion  
    pc_send_to_pacs --send_diffusion  
    pc_send_to_pacs --send_sivic      
    pc_send_to_pacs --send_axial
