#!/netopt/bin/local/prostate/python27/bin/python2.7

#   $URL$
#   $Rev$
#   $Author$
#   $Date$
#


import os
import dicom    as dcm
import argparse as ap
from   multiprocessing.dummy import Pool as ThreadPool
from   pc_utils import mkdirs, natural_key, gunzip_to_temp, change_permissions
from   uid      import unique_uid


class FilteredDicomSeries(object):
    """Base class to take a dicom series, and creates a new series, based off of a user
    defined filter. To be refactored/generalized..."""
    def __init__(self, series_directory, destination):
        super(FilteredDicomSeries, self).__init__()
        series_directory    = os.path.abspath(series_directory)
        self.old_directory  = series_directory
        self.exam_directory = os.path.dirname(self.old_directory)
        self.dce_combined   = os.path.abspath(destination)
        self.meta           = {}
        self.ds             = None
        self.dicom_list     = [os.path.join(self.old_directory, d) for d in
                               os.listdir(self.old_directory) if '.dcm' in d.lower()]
        self.dicom_list     = sorted(self.dicom_list, key=natural_key)
        if self.dicom_list:
            if self.dicom_list[0][-3:].lower() == '.gz':
                self.old_directory = gunzip_to_temp(series_directory)
                self.dicom_list    = [os.path.join(self.old_directory, d) for d in
                                      os.listdir(self.old_directory) if '.dcm' in d.lower()]
            self.load_ds()
            self.set_meta_data()

    def load_ds(self):
        self.ds = dcm.read_file(self.dicom_list[0])

    def get_timing_and_locations(self):
        self.slice_locations = set()
        self.trigger_times   = set()
        for image in self.dicom_list:
            ds = dcm.read_file(image)
            self.slice_locations.add(ds.SliceLocation)
            if 'TriggerTime' in ds:
                self.trigger_times.add(int(ds.TriggerTime))
        trigger_time_list         = sorted(self.trigger_times)
        self.temporal_resolution  = trigger_time_list[2] - trigger_time_list[1]
        self.number_of_images     = int(self.ds.ImagesInAcquisition)
        self.number_of_timepoints = int(self.ds.NumberOfTemporalPositions)
        self.number_of_slices     = len(self.slice_locations)

    def get_timing_and_locations_multi(self, num_threads=10):
        self.slice_locations = set()
        self.trigger_times   = set()

        def worker(image):
            ds = dcm.read_file(image)
            self.slice_locations.add(ds.SliceLocation)
            if 'TriggerTime' in ds:
                self.trigger_times.add(int(ds.TriggerTime))

        pool = ThreadPool(num_threads)
        pool.map(worker, self.dicom_list)
        pool.close()
        pool.join()
        trigger_time_list         = sorted(self.trigger_times)
        self.temporal_resolution  = trigger_time_list[2] - trigger_time_list[1]
        self.number_of_images     = int(self.ds.ImagesInAcquisition)
        self.number_of_timepoints = int(self.ds.NumberOfTemporalPositions)
        self.number_of_slices     = len(self.slice_locations)

    def set_temporal_resolution_from_temporal_position(self):
        temporal_res_list = []
        slice_positions   = set()
        try:
            for image in self.dicom_list:
                ds = dcm.read_file(image)
                slice_positions.add(ds.SliceLocation)
                if str(ds.TemporalPositionIdentifier) == '2':
                    temporal_res_list.append(int(ds.TriggerTime))
        except Exception:
            temporal_resolution = None
        else:
            if len(temporal_res_list) == len(slice_positions):
                temporal_res_list.sort()
                temporal_resolution = temporal_res_list[0]
            else:
                temporal_resolution = None
        finally:
            self.temporal_res = temporal_resolution

    def set_number_of_coils(self):
        self.number_of_coils = self.number_of_images / self.number_of_timepoints / self.number_of_slices
        if self.number_of_coils > 1:
            self.number_of_coils -= 1

    def filter_on_coils(self):
        skip = self.number_of_coils
        if self.number_of_coils != 1:
            skip += 1

        def coil_filter(filename):
            base_name = filename.split('.')[0]
            try:
                instance = int(base_name.split('I')[1])
            except Exception:
                return False
            else:
                if instance % skip == 0:
                    return True
                else:
                    return False
        self.dicom_list = [d for d in self.dicom_list if coil_filter(os.path.basename(d))]

    def set_meta_data(self):
        if 'pet-mr' in self.ds.ProtocolName.lower():
            jump = 10000
        else:
            jump = 1000
        self.meta['series_number']      = str(int(self.ds.SeriesNumber) + jump)
        self.meta['study_id']           = self.ds.StudyID
        self.meta['filename_base']      = 'E' + self.meta['study_id'] + 'S' + self.meta['series_number'] + 'I'
        self.meta['series_description'] = self.ds.SeriesDescription + '-CombinedCoil'
        self.meta['series_uid']         = unique_uid()

    def write_new_series(self):
        # series_path = os.path.join(self.exam_directory, self.meta['series_number'])
        series_path = os.path.join(self.dce_combined, self.meta['series_number'])
        last_uid    = self.meta['series_uid']
        mkdirs(series_path)
        for idx, image in enumerate(self.dicom_list):
            ds                   = dcm.read_file(image)
            ds.SeriesNumber      = self.meta['series_number']
            ds.SeriesDescription = self.meta['series_description']
            ds.SeriesInstanceUID = self.meta['series_uid']
            ds.InstanceNumber    = idx + 1
            new_uid              = unique_uid()
            assert new_uid      != last_uid
            ds.SOPInstanceUID    = last_uid = new_uid
            if self.temporal_resolution:
                ds.TemporalResolution = self.temporal_resolution
            filename             = self.meta['filename_base'] + str(ds.InstanceNumber) + '.DCM'
            save_path            = os.path.join(series_path, filename)
            ds.save_as(save_path)
        return series_path


def combine_dce(exam_directory, series_list, destination):
    exam_directory = os.path.abspath(exam_directory)
    created        = []
    for series in series_list:
        series_directory = os.path.join(exam_directory, str(series))
        if os.path.isdir(series_directory):
            try:
                filtered_series = FilteredDicomSeries(series_directory, destination)
                filtered_series.get_timing_and_locations()
                filtered_series.set_number_of_coils()
                filtered_series.filter_on_coils()
                new_series = filtered_series.write_new_series()
                change_permissions(new_series)
            except Exception, e:
                print '\tCould not create combined coil image for: %s' % series
                print e
            else:
                print '\tCreated: %s' % new_series
                created.append(new_series)
        else:
            print 'Could not find %s' % series_directory
    return created


def main(exam_directory, series_list, destination):
    args = arg_set_up()
    combine_dce(args.exam, args.series, args.num_coils)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Takes a list of preGd and DCE dicom series, acquired with multiple coils, and
    pulls out the combined coil images. Creates new dicom series, in the exam
    directory, with the series number 1000 + base series number, and adds
    '-CombinedCoil' to the series description. Defaults to 8 coils, and assumes
    every 9th instance is the combined image. Takes TriggerTime from the first
    image aquired in the 2nd Temporal Position, and writes that as the new series'
    Temporal Resolution.
    """)
    requiredNamed = parser.add_argument_group("required named arguments")
    requiredNamed.add_argument('-e', '--exam',
                               required = True,
                               help     = "the prostate exam directory containing series to convert")
    requiredNamed.add_argument('-s', '--series',
                               dest     = "series",
                               required = True,
                               type     = int,
                               nargs    = "+",
                               help     = "a list of series numbers to convert")

    return parser.parse_args()

if __name__ == '__main__':
    main()
