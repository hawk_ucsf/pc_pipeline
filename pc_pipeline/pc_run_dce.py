#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import shutil         as sh
import argparse       as ap
import subprocess     as sub
import fnmatch
import dicom          as dcm
from   collections    import namedtuple
from   pc_pipe        import ProstatePipeline
from   pc_combine_dce import combine_dce
from   pc_utils       import mkdirs, change_permissions, natural_key,\
                             ucsf_log_append, sym_link
from   pc_data        import ProstateExam
import logging        as lg


SVK_RESLICE           = '/netopt/bin/local/svk_reslice'
SVK_IMAGE_MATHEMATICS = '/netopt/bin/local/svk_image_mathematics'
SVK_TRANSFORM         = '/netopt/bin/local/svk_transform'
SVK_DCE_QUANTIFY      = '/netopt/bin/local/svk_dce_quantify'

SVK_RESLICE_DEV           = '/netopt/bin/local/svk_reslice.dev'
SVK_IMAGE_MATHEMATICS_DEV = '/netopt/bin/local/svk_image_mathematics.dev'
SVK_TRANSFORM_DEV         = '/netopt/bin/local/svk_transform.dev'
SVK_DCE_QUANTIFY_DEV      = '/netopt/bin/local/svk_dce_quantify.dev'
# SVK_DCE_QUANTIFY_DEV      = '/home/jhawkins/sivic/applications/cmd_line/Linux_x86_64/svk_dce_quantify'


class ProstateDCEValidator(ProstateExam):
    """
    A simple validator, based off of pc_get_data's ProstateExam. Uses the
    inherited DCE behavior, and leaves off the rest.
    """

    def __init__(self, exam_directory):
        super(ProstateDCEValidator, self).__init__(exam_directory)

    def get_relevant_series(self):
        """Pares down relevant series builder to just DCE series."""
        self.dce_series = []
        for idx, series in enumerate(self.series):
            series_tup = idx, series['Series'], series['Description']
            if self.is_dce(series):
                self.dce_series.append(series_tup)

    def validate(self):
        """Runs only the DCE validation."""
        check = self.check_dce()
        valid = True if check else False
        if not valid:
            self.failures = {}
            self.failures['dce'] = [(key, val) for (key, val) in
                                    self.valid_dce.iteritems()
                                    if not val]
        return valid


class ProstateDCEPipeline(ProstatePipeline):
    """
    Implements pc_run_pipe's ProstatePipeline, with ProstateDCEValidator as its
    validator. Runs through these steps:
        - Creates perfusion directory structure
        - Creates a combined coil image using pc_combine_dce, and pulls out timepoints
          from preGd and DCE images
        - Fits DCE using svk_dce_quantify
        - Normalizes curves using svk_image_mathematics
        - Downsamples curves using svk_reslice
        - Shifts DCE centerpoint using svk_transform
        - Symlinks relevant results to review_images/
    """

    def __init__(self, shift_list=None, pc_num=None, pipe_logger=None):
        super(ProstateDCEPipeline, self).__init__(pc_num=pc_num, pipe_logger=pipe_logger)
        self.shift_list = shift_list

    def set_validator(self):
        self.validator = ProstateDCEValidator

    def create_combined_dce_dicoms(self, exam_path, series_num_list):
        created = combine_dce(exam_path, series_num_list)
        return created

    def set_up_directories(self, combined_series_list=[]):
        dce_combined_dicom  = os.path.join(self.pc_directory, 'DCE_combined_DICOM')
        perfusion_directory = os.path.join(self.pc_directory, 'perfusion')
        review_directory    = os.path.join(self.pc_directory, 'review_images')
        dcm_output          = os.path.join(perfusion_directory, 'dcm_output')
        downsampled         = os.path.join(perfusion_directory, 'dynamic_data_downsampled')
        shifted_dce         = os.path.join(perfusion_directory, 'shifted_dce')
        normalized_dce      = os.path.join(perfusion_directory, 'normalized_dce')
        mkdirs(dce_combined_dicom)
        mkdirs(review_directory)
        mkdirs(dcm_output)
        mkdirs(downsampled)
        mkdirs(shifted_dce)
        mkdirs(normalized_dce)
        for series in combined_series_list:
            series_num  = os.path.basename(series)
            series_dest = os.path.join(dce_combined_dicom, series_num)
            if os.path.exists(series_dest):
                sh.rmtree(series_dest)
            sh.copytree(series, series_dest)
        change_permissions(dce_combined_dicom)
        change_permissions(perfusion_directory)
        change_permissions(review_directory)
        Directories = namedtuple('Directories', ['dce_combined_dicom',
                                                 'review_directory',
                                                 'perfusion_directory',
                                                 'dcm_output',
                                                 'downsampled',
                                                 'normalized_dce',
                                                 'shifted_dce'])
        self.directories = Directories(dce_combined_dicom,
                                       review_directory,
                                       perfusion_directory,
                                       dcm_output,
                                       downsampled,
                                       normalized_dce,
                                       shifted_dce)
        return self.directories

    def pull_timepoint(self, series, timepoint):
        if type(timepoint) != int:
            raise TypeError
        series_num     = os.path.basename(series)
        exam_path      = os.path.dirname(series)
        timepoint_path = os.path.join(exam_path, series_num + '_t' + str(timepoint))
        dicom_list     = sorted([f for f in os.listdir(series) if '.dcm' in f.lower()])
        dicom_list     = [os.path.join(series, f) for f in dicom_list]
        mkdirs(timepoint_path)
        for f in dicom_list:
            ds = dcm.read_file(f)
            if str(ds.TemporalPositionIdentifier) == str(timepoint):
                sh.copy(f, timepoint_path)
        new_images = os.listdir(timepoint_path)
        if new_images:
            print '\tCreated: %s' % timepoint_path
            return timepoint_path
        else:
            os.rmdir(timepoint_path)

    def create_combined_coils_dce(self):
        self.logger.info('Creating combined coil images...')
        directories = self.set_up_directories()
        try:
            combined = []
            for exam in self.exam_validators:
                if exam.dce_series:
                    exam_num    = 'E' + exam.exam['Exam']
                    exam_path   = os.path.join(self.pc_directory, exam_num)
                    if 'pet' in exam.exam['Station']['Model'].lower():
                        series_list = [s[1] for s in exam.dce_series]
                    else:
                        series_list = [s[1] for s in exam.dce_series if len(str(s[1])) <= 2]
                    created = combine_dce(exam_path, series_list, directories.dce_combined_dicom)
                    combined.extend(created)
            # directories     = self.set_up_directories(combined)
            dce_series_list = sorted(os.listdir(directories.dce_combined_dicom))
            dce_series_list = [os.path.join(directories.dce_combined_dicom, d) for d
                               in dce_series_list if '_t' not in d]
            created_series  = []
            created_series.extend(dce_series_list)
            for dce_series in dce_series_list:
                created_series.append(self.pull_timepoint(dce_series, 5))
                created_series.append(self.pull_timepoint(dce_series, 20))
            # put dce_t5 in review_images
            dce_series = self.set_up_dce_data()
            dce_t5     = dce_series + '_t5'
            if os.path.isdir(dce_t5):
                series_name = self.pc_num + '_dce_t5'
                sym_link(dce_t5, self.directories.review_directory, link_name=series_name)
        except Exception, e:
            self.logger.info('Error creating combined coil DCE series')
            change_permissions(self.directories.dce_combined_dicom)
            raise e
        else:
            change_permissions(self.directories.dce_combined_dicom)
            return created_series

    def set_up_dce_data(self):
        """Returns DCE series from DCE_combined_DICOM/"""
        dce_series = sorted(os.listdir(self.directories.dce_combined_dicom))
        if dce_series:
            dce_series = [d for d in dce_series if int_filter(d)][-1]
            dce_series = os.path.join(self.directories.dce_combined_dicom, dce_series)
            return dce_series
        else:
            return None

    def fit_dce(self, dce_series, output_type=5):
        self.logger.info('Generating DCE maps...')
        try:
            if os.path.isdir(dce_series):
                series_list = os.listdir(dce_series)
                dicom_list  = [d for d in series_list if '.dcm' in d.lower()]
                dicom_list  = sorted(dicom_list, key=natural_key)
                first_dcm   = os.path.join(dce_series, dicom_list[0])
            else:
                first_dcm = dce_series
            output_root  = os.path.join(self.directories.dcm_output, self.pc_num)
            command      = [SVK_DCE_QUANTIFY_DEV, '-i', first_dcm, '-o', output_root, '-t', str(output_type)]
            result       = sub.check_call(command)
            base_dir     = os.path.join(output_root + '_base_ht')
            peak_dir     = os.path.join(output_root + '_peak_ht')
            time_dir     = os.path.join(output_root + '_peak_time')
            slope_dir    = os.path.join(output_root + '_slope')
            wash_dir     = os.path.join(output_root + '_washout')
            wash_pos_dir = os.path.join(output_root + '_washout_pos')
            mkdirs(base_dir)
            mkdirs(peak_dir)
            mkdirs(time_dir)
            mkdirs(slope_dir)
            mkdirs(wash_dir)
            mkdirs(wash_pos_dir)
            ds            = dcm.read_file(first_dcm)
            series_number = int(str(ds.SeriesNumber)[-2:])
            series_number = series_number * 100
            for pair in [(base_dir,     '*base_htI*',     'DCE Base Value Params', None),
                         (peak_dir,     '*peak_htI*',     'DCE Peak Value Params', series_number + 11),
                         (time_dir,     '*timeI*',        'DCE Peak Time Params',  series_number + 10),
                         (slope_dir,    '*slopeI*',       'DCE Up Slope Params',   series_number + 12),
                         (wash_dir,     '*washoutI*',     'DCE Washout Params',    None),
                         (wash_pos_dir, '*washout_posI*', 'DCE Washout Params',    series_number + 13)]:
                for f in os.listdir(self.directories.dcm_output):
                    if fnmatch.fnmatch(f, pair[1]):
                        src        = os.path.join(self.directories.dcm_output, f)
                        dst_dir    = pair[0]
                        dst_exists = os.path.join(dst_dir, f)
                        self.set_series_description(src, pair[2])
                        if pair[3]:
                            self.set_series_number(src, pair[3])
                        if os.path.exists(dst_exists):
                            os.remove(dst_exists)
                        sh.move(src, dst_dir)
            for d in [peak_dir, slope_dir, wash_dir]:
                link = os.path.join('..', d.split(self.pc_num + '/')[1])
                sym_link(link, self.directories.review_directory)
        except Exception, e:
            self.logger.info('Error creating DCE maps')
            change_permissions(self.directories.dcm_output)
            raise e
        else:
            change_permissions(self.directories.dcm_output)
            return result

    def normalize(self, dce_series, base_series):
        self.logger.info('Normalizing DCE series...')
        try:
            if os.path.isdir(dce_series):
                series_list   = os.listdir(dce_series)
                dicom_list    = [d for d in series_list if '.dcm' in d.lower()]
                dicom_list    = sorted(dicom_list, key=natural_key)
                first_dce_dcm = os.path.join(dce_series, dicom_list[0])
            else:
                first_dce_dcm = dce_series
            if os.path.isdir(base_series):
                series_list    = os.listdir(base_series)
                dicom_list     = [d for d in series_list if '.dcm' in d.lower()]
                dicom_list     = sorted(dicom_list, key=natural_key)
                first_base_dcm = os.path.join(base_series, dicom_list[0])
            else:
                first_base_dcm = base_series
            input_root  = os.path.basename(first_dce_dcm)
            input_base  = input_root.split('.')[0]
            output_base = input_base
            output_root = os.path.join(self.directories.normalized_dce, output_base)
            command     = [SVK_IMAGE_MATHEMATICS_DEV, '--i1', first_dce_dcm,
                                                      '--i2', first_base_dcm,
                                                      '-p', '4',
                                                      '-o', output_root,
                                                      '--output_type', '2']
            result      = sub.check_call(command)
            # command     = [SVK_IMAGE_MATHEMATICS, '--i1', 'norm_tmp_' + output_root + '1.dcm',
            #                                       '-p', '5',
            #                                       '-o', 'normalized_' + output_root,
            #                                       '-s', '1000']
            # result      = sub.check_call(command)
        except Exception, e:
            self.logger.info('Error normalizing DCE curves')
            change_permissions(self.directories.normalized_dce)
            raise e
        else:
            change_permissions(self.directories.normalized_dce)
            return result

    def downsample(self, series, output_dir, output_type=5):
        self.logger.info('Downsampling DCE series...')
        try:
            if os.path.isdir(series):
                series_list = os.listdir(series)
                dicom_list  = [d for d in series_list if '.dcm' in d.lower()]
                dicom_list  = sorted(dicom_list, key=natural_key)
                first_dcm   = os.path.join(series, dicom_list[0])
            else:
                first_dcm = series
            input_root  = os.path.basename(first_dcm)
            input_base  = input_root.split('.')[0]
            output_base = input_base
            output_root = os.path.join(output_dir, output_base)
            command     = [SVK_RESLICE, '--mx', '2',
                                        '--my', '2',
                                        '-i', first_dcm,
                                        '--target', first_dcm,
                                        '-o', output_root,
                                        '-t', str(output_type)]
            result      = sub.check_call(command)
        except Exception, e:
            self.logger.info('Error downsamplling DCE curves')
            change_permissions(self.directories.downsampled)
            raise e
        else:
            change_permissions(self.directories.downsampled)
            return result

    def shift_centers(self, series, output_type=5):
        self.logger.info('Shifting DCE series...')
        try:
            if os.path.isdir(series):
                series_list = os.listdir(series)
                dicom_list  = [d for d in series_list if '.dcm' in d.lower()]
                dicom_list  = sorted(dicom_list, key=natural_key)
                first_dcm   = os.path.join(series, dicom_list[0])
            else:
                first_dcm = series
            input_root  = os.path.basename(first_dcm)
            input_base  = input_root.split('.')[0]
            output_base = input_base.split('I')[0]
            output_root = os.path.join(self.directories.shifted_dce, output_base)
            command     = [SVK_TRANSFORM, '--dl', str(self.shift_list[0]),
                                          '--dp', str(self.shift_list[1]),
                                          '--ds', str(self.shift_list[2]),
                                          '-i', first_dcm,
                                          '-o', output_root,
                                          '-t', str(output_type)]
            result      = sub.check_call(command)
        except Exception, e:
            self.logger.info('Error shifting DCE center')
            change_permissions(self.directories.shifted_dce)
            raise e
        else:
            change_permissions(self.directories.shifted_dce)
            return True

    def run(self):
        self.logger.info('Running DCE workflow...')
        self.set_up_directories()
        dce_series = self.set_up_dce_data()
        if not dce_series:
            self.create_combined_coils_dce()
            dce_series = self.set_up_dce_data()
        if self.shift_list:
            if self.shift_centers(dce_series):
                dce_series = self.directories.shifted_dce
        self.fit_dce(dce_series)
        baseline_series = os.path.join(self.directories.dcm_output, self.pc_num + '_base_ht')
        self.normalize(dce_series, baseline_series)
        self.downsample(self.directories.normalized_dce, self.directories.downsampled)


def int_filter(value):
    """Helper function to determine if string is an integer."""
    try:
        int(value)
    except ValueError:
        return False
    else:
        return True


def run_dce(shift_list=None, pc_num=None, pipe_logger=None):
    test = ProstateDCEPipeline(shift_list=shift_list, pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.run()


def run_coil(pc_num=None, pipe_logger=None):
    test = ProstateDCEPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.create_combined_coils_dce()


def run_fit(shift_list=None, pc_num=None, pipe_logger=None):
    test = ProstateDCEPipeline(shift_list=shift_list, pc_num=pc_num, pipe_logger=pipe_logger)
    test.set_up_directories()
    dce_series = test.set_up_dce_data()
    if not dce_series:
        raise Exception("Did not find a DCE series in DCE_combined_DICOM/")
    if test.shift_list:
        if test.shift_centers(dce_series):
            dce_series = test.directories.shifted_dce
    test.fit_dce(dce_series)


def run_curves(shift_list=None, pc_num=None, pipe_logger=None):
    test = ProstateDCEPipeline(shift_list=shift_list, pc_num=pc_num, pipe_logger=pipe_logger)
    test.set_up_directories()
    dce_series = test.set_up_dce_data()
    if not dce_series:
        raise Exception("Did not find a DCE series in DCE_combined_DICOM/")
    baseline_series = os.path.join(test.directories.dcm_output, test.pc_num + '_base_ht')
    if not os.listdir(baseline_series):
        raise Exception("Did not find a base height series in perfusion/dcm_output/")
    test.normalize(dce_series, baseline_series)
    test.downsample(test.directories.normalized_dce, test.directories.downsampled)


def run_fit_and_curves(shift_list=None, pc_num=None, pipe_logger=None):
    test = ProstateDCEPipeline(shift_list=shift_list, pc_num=pc_num, pipe_logger=pipe_logger)
    test.set_up_directories()
    dce_series = test.set_up_dce_data()
    if not dce_series:
        raise Exception("Did not find a DCE series in DCE_combined_DICOM/")
    if test.shift_list:
        if test.shift_centers(dce_series):
            dce_series = test.directories.shifted_dce
    test.fit_dce(dce_series)
    baseline_series = os.path.join(test.directories.dcm_output, test.pc_num + '_base_ht')
    if not os.listdir(baseline_series):
        raise Exception("Did not find a base height series in perfusion/dcm_output/")
    test.normalize(dce_series, baseline_series)
    test.downsample(test.directories.normalized_dce, test.directories.downsampled)


def main():
    from pc_run_review import run_review

    logger           = lg.getLogger('run_dce')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()

    try:
        local_log = lg.FileHandler('run_dce.log')
        local_log.setFormatter(file_formatter)
        local_log.setLevel(lg.DEBUG)
        logger.addHandler(local_log)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        shift_list = []
        proceed    = True
        if args.center_points:
            if len(args.center_points) != 6:
                raise Exception('Please supply 6 center point values')
            shift_list, proceed = get_shift_list(args.center_points, logger)
        if proceed:
            if args.run_coil:
                run_coil(pc_num=args.pc_num, pipe_logger=logger)
            elif args.run_maps:
                run_fit(shift_list=shift_list, pc_num=args.pc_num, pipe_logger=logger)
            elif args.run_curves:
                run_curves(shift_list=shift_list, pc_num=args.pc_num, pipe_logger=logger)
            elif args.run_maps_and_curves:
                run_fit_and_curves(shift_list=shift_list, pc_num=args.pc_num, pipe_logger=logger)
            else:
                run_dce(shift_list=shift_list, pc_num=args.pc_num, pipe_logger=logger)
        run_review(pc_num=args.pc_num, pipe_logger=logger)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Kicks off the DCE fitting workflow, when run in a pc#### directory, created by
    pc_get_data. Will run: svk_transform (if the optional centerpoints are supplied),
    svk_dce_quantify, and svk_reslice. Reads in DCE data from the highest-numbered series
    in the DCE_combined_DICOM directory. Results are written to perfusion/dcm_output.
    Auto-detects pc_number from directory.
    """)
    parser.add_argument('-c', '--center_points',
                        dest    = "center_points",
                        type    = float,
                        nargs   = "+",
                        help    = "6 values representing the T2 and DCE center points, in LPS order")
    parser.add_argument('--pc_num',
                        dest    = "pc_num",
                        help    = "optional pc_number to use in map naming, i.e. pc1234")
    parser.add_argument('--combo_coil',
                        dest    = "run_coil",
                        action  = "store_true",
                        default = False,
                        help    = "create combined coil dicom series in DCE_combined_DICOM/")
    parser.add_argument('--maps',
                        dest    = "run_maps",
                        action  = "store_true",
                        default = False,
                        help    = "run only DCE map generation step")
    parser.add_argument('--curves',
                        dest    = "run_curves",
                        action  = "store_true",
                        default = False,
                        help    = "run only DCE curve normalization and downsampling steps")
    parser.add_argument('--maps_and_curves',
                        dest    = "run_maps_and_curves",
                        action  = "store_true",
                        default = False,
                        help    = "run map and curve steps")
    return parser.parse_args()


def get_shift_list(center_points, logger):
    """
    Formats centerpoint command line input for use with svk_transform, and
    confirms with user.
    """
    t2_center  = center_points[:3]
    dce_center = center_points[3:]
    shift_l    = t2_center[0] - dce_center[0]
    shift_p    = t2_center[1] - dce_center[1]
    shift_s    = t2_center[2] - dce_center[2]
    if shift_l != 0.0 or shift_p != 0.0 or shift_s != 0.0:
        logger.info('T2 Center: %s', t2_center)
        logger.info('DCE Center: %s', dce_center)
        logger.info('LPS Shift: %s,', [shift_l, shift_p, shift_s])
        while True:
            logger.info('Proceed with shift ([y]/n)?')
            choice = raw_input().lower()
            if choice in ['', 'y', 'yes']:
                shift_list = [shift_l, shift_p, shift_s]
                return (shift_list, True)
            elif choice in ['n', 'no']:
                logger.info('Exiting...')
                return ([], False)

if __name__ == '__main__':
    main()
