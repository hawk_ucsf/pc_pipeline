#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import shutil      as sh
import argparse    as ap
import subprocess  as sub
import dicom       as dcm
from   fnmatch     import fnmatch
from   collections import namedtuple
from   pc_pipe     import ProstatePipeline
from   pc_utils    import mkdirs, change_permissions, ucsf_log_append,\
                          natural_key, sym_link
from   pc_data     import ProstateExam
import logging     as lg


PC_DIFF_MEAN_DEV = '/netopt/bin/local/prostate/pc_diff_mean.dev'
PC_DIFF_ADC_DEV  = '/netopt/bin/local/prostate/pc_diff_adc.dev'
DIFFU_DICOM      = '/netopt/bin/local/diffu_dicom'


class ProstateADCValidator(ProstateExam):
    """
    A simple validator, based off of pc_get_data's ProstateExam. Uses the
    inherited ADC behavior, and leaves off the rest. Overrides check_adc() to
    categorize the different DWI acquisitions, and log which types are available
    for ProstateADCPipeline to process.
    """

    def __init__(self, exam_directory):
        super(ProstateADCValidator, self).__init__(exam_directory)

    def get_relevant_series(self):
        """Pares down relevant series builder to just DWI series."""
        self.adc_series = []
        for idx, series in enumerate(self.series):
            series_tup = idx, series['Series'], series['Description']
            if self.is_dwi(series):
                self.adc_series.append(series_tup)

    def validate(self):
        """Runs only the ADC validation"""
        check = self.check_adc()
        valid = True if check else False
        if not valid:
            self.failures = {}
            self.failures['adc'] = [(key, val) for (key, val) in
                                    self.valid_adc.iteritems()
                                    if not val]
        return valid

    def check_adc(self):
        """
        Looks for the different DWI series types, and stores their paths in a
        dictionary for the ProstateADCPipeline to access by type.
        """
        def is_rfov(description):
            for desc in self.relevant_dict['dwi']['rfov']:
                if fnmatch(description.lower(), desc):
                    return True

        def is_high_b(description):
            for desc in self.relevant_dict['dwi']['high-b']:
                if fnmatch(description.lower(), desc):
                    return True

        def is_high_b_rfov(description):
            for desc in self.relevant_dict['dwi']['high-b rfov']:
                if fnmatch(description.lower(), desc):
                    return True

        def is_std(description):
            for desc in self.relevant_dict['dwi']['std']:
                if fnmatch(description.lower(), desc):
                    return True

        def is_std_brachy(description):
            for desc in self.relevant_dict['dwi']['std brachy']:
                if fnmatch(description.lower(), desc):
                    return True

        self.adc = {}
        res      = {
                    'rfov':           False,
                    'rfov_brachy':    False,
                    'high-b':         False,
                    'high-b_cc':      False,
                    'high-b_rfov':    False,
                    'high-b_rfov_cc': False,
                    'std':            False,
                    'std_brachy':     False,
                   }
        for series_tup in self.adc_series:
            idx, num, desc = series_tup
            series         = self.series[idx]

            if is_rfov(desc):
                if 'pt with seeds' in desc.lower():
                    res['rfov_brachy']      = True
                    self.adc['rfov_brachy'] = series['/Path']
                else:
                    res['rfov']      = True
                    self.adc['rfov'] = series['/Path']
            elif is_high_b(desc):
                if len(str(num)) <= 2:
                    res['high-b']      = True
                    self.adc['high-b'] = series['/Path']
                else:
                    res['high-b_cc']      = True
                    self.adc['high-b_cc'] = series['/Path']
            elif is_high_b_rfov(desc):
                if len(str(num)) <= 2:
                    res['high-b']           = True
                    self.adc['high-b_rfov'] = series['/Path']
                else:
                    res['high-b_cc']           = True
                    self.adc['high-b_rfov_cc'] = series['/Path']
            elif is_std_brachy(desc):
                res['std_brachy']      = True
                self.adc['std_brachy'] = series['/Path']
            elif is_std(desc):
                res['std']      = True
                self.adc['std'] = series['/Path']

        self.valid_adc = res
        check          = [val for (key, val) in res.iteritems()]
        valid          = True if True in check else False
        return valid


class ProstateADCPipeline(ProstatePipeline):
    """
    Implements pc_run_pipe's ProstatePipeline, with ProstateADCValidator as its
    validator. Runs through these steps, using the validator to determine which
    diffusion steps to perform:
        - Creates tensor directory
        - Runs appropriate steps for each DWI series type stored in the validator
        - Calculates ADC map using pc_diff_adc
        - Calculates either geometric mean for average map with pc_diff_mean
        - Symlinks relevant results to review_images/
    """

    def __init__(self, shift_list=None, pc_num=None, pipe_logger=None):
        super(ProstateADCPipeline, self).__init__(pc_num=pc_num, pipe_logger=pipe_logger)

    def set_validator(self):
        self.validator = ProstateADCValidator

    def set_up_directories(self):
        tensor_dir = os.path.join(self.pc_directory, 'tensor')
        review_dir = os.path.join(self.pc_directory, 'review_images')
        mkdirs(tensor_dir)
        mkdirs(review_dir)
        change_permissions(tensor_dir)
        change_permissions(review_dir)
        Directories      = namedtuple('Directories', ['tensor_directory', 'review_directory'])
        self.directories = Directories(tensor_dir, review_dir)
        return self.directories

    def get_slices_and_phases(self, series_directory):
        """
        Finds number of slices and phases (4d volumes) of a series. Legacy method,
        since pc_diff_adc and pc_diff_mean pull this information automatically.
        """
        slice_locations = set()
        dicom_list      = [os.path.join(series_directory, d)
                           for d in os.listdir(series_directory)
                           if '.dcm' in d.lower()]
        dicom_list      = sorted(dicom_list, key=natural_key)
        for image in dicom_list:
            ds = dcm.read_file(image)
            slice_locations.add(ds.SliceLocation)
        number_of_images = int(ds.ImagesInAcquisition)
        number_of_slices = len(slice_locations)
        number_of_phases = number_of_images / number_of_slices
        return number_of_slices, number_of_phases

    def _calculate_adc(self, dwi_series, dwi_type='', b0=2):
        self.logger.info('Generating ADC map...')
        try:
            dwi_series  = self._get_image_series_input(dwi_series)
            output_base = self.pc_num + '_' + dwi_type + '_adc'
            output_root = os.path.join(self.directories.tensor_directory, output_base)
            mkdirs(output_root)
            output_root = os.path.join(output_root, output_base)
            command     = [PC_DIFF_ADC_DEV, '-i',   dwi_series,
                                            '-o',   output_root,
                                            '--b0', str(b0)]
            result      = sub.check_call(command)
            dirname     = os.path.dirname(output_root)
            link        = os.path.join('..', dirname.split(self.pc_num + '/')[1])
            sym_link(link, self.directories.review_directory)
            ds            = dcm.read_file(dwi_series)
            series_number = int(ds.SeriesNumber)
            series_number = series_number * 1100 - 1000
            self.set_series_number(dirname, series_number)
            if 'rfov' in dwi_type:
                description = 'rFOV Apparent Diffusion Coefficient Map'
                series_dir  = os.path.join(self.directories.tensor_directory, output_base)
                self.set_series_description(series_dir, description)
        except Exception, e:
            self.logger.info('Error Generating ADC map')
            change_permissions(self.directories.tensor_directory)
            raise e
        else:
            change_permissions(self.directories.tensor_directory)
            return result

    def _calculate_geo_mean(self, dwi_series, dwi_type='', skip=2):
        self.logger.info('\tGenerating Geo Mean map...')
        try:
            dwi_series  = self._get_image_series_input(dwi_series)
            output_base = self.pc_num + '_' + dwi_type + '_geo'
            output_root = os.path.join(self.directories.tensor_directory, output_base)
            mkdirs(output_root)
            output_root = os.path.join(output_root, output_base)
            command     = [PC_DIFF_MEAN_DEV, '-i',     dwi_series,
                                             '-o',     output_root,
                                             '--skip', str(skip),
                                             '--geo']
            result      = sub.check_call(command)
            dirname     = os.path.dirname(output_root)
            link        = os.path.join('..', dirname.split(self.pc_num + '/')[1])
            sym_link(link, self.directories.review_directory)
        except Exception, e:
            self.logger.info('\tError Generating Geo Mean map')
            change_permissions(self.directories.tensor_directory)
            raise e
        else:
            change_permissions(self.directories.tensor_directory)
            return result

    def _calculate_avg(self, dwi_series, dwi_type='', skip=2):
        self.logger.info('\tGenerating Avg map...')
        try:
            dwi_series  = self._get_image_series_input(dwi_series)
            output_base = self.pc_num + '_' + dwi_type + '_avg'
            output_root = os.path.join(self.directories.tensor_directory, output_base)
            mkdirs(output_root)
            output_root = os.path.join(output_root, output_base)
            command     = [PC_DIFF_MEAN_DEV, '-i',     dwi_series,
                                             '-o',     output_root,
                                             '--skip', str(skip)]
            result      = sub.check_call(command)
            dirname     = os.path.dirname(output_root)
            link        = os.path.join('..', dirname.split(self.pc_num + '/')[1])
            sym_link(link, self.directories.review_directory)
            if 'high-b' in dwi_type:
                description = 'DWI-1350 AVG Diffusion'
                series_dir  = os.path.join(self.directories.tensor_directory, output_base)
                self.set_series_description(series_dir, description)
                ds            = dcm.read_file(dwi_series)
                series_number = int(ds.SeriesNumber)
                series_number = series_number * 10 + 50
                self.set_series_number(dirname, series_number)
        except Exception, e:
            self.logger.info('\tError Generating Avg map')
            change_permissions(self.directories.tensor_directory)
            raise e
        else:
            change_permissions(self.directories.tensor_directory)
            return result

    def _run_diffu_dicom(self, dwi_series, dwi_type=''):
        self.logger.info('\tGenerating ADC map with diffu_dicom...')
        try:
            # dwi_series  = self._get_image_series_input(dwi_series)
            dest_base_adc = self.pc_num + '_' + dwi_type + '_adc'
            dest_root_adc = os.path.join(self.directories.tensor_directory, dest_base_adc)
            mkdirs(dest_root_adc)
            output_root_1 = os.path.join(os.getcwd(), '1')
            command = [DIFFU_DICOM, '-B',
                                    '-m', '0',
                                    '-o', '6',
                                    '-b', '600',
                                    '-f1',
                                    '-s1',
                                    '-d',
                                    dwi_series]
            result  = sub.check_call(command)
            for f in os.listdir(output_root_1):
                src = os.path.join(output_root_1, f)
                sh.copy2(src, dest_root_adc)
            sh.rmtree(output_root_1, ignore_errors=True)
            link = os.path.join('..', dest_root_adc.split(self.pc_num + '/')[1])
            sym_link(link, self.directories.review_directory)
            dwi_series    = self._get_image_series_input(dwi_series)
            ds            = dcm.read_file(dwi_series)
            series_number = int(ds.SeriesNumber)
            series_number = series_number * 1100 - 1000
            self.set_series_number(dest_root_adc, series_number)
        except Exception, e:
            self.logger.info('\tError Generating ADC map with diffu_dicom')
            change_permissions(self.directories.tensor_directory)
            raise e
        else:
            change_permissions(self.directories.tensor_directory)
            return result

    def run_high_b(self, b0=2):
        self.logger.info('Processing High-B...')
        dwi_type   = 'high-b_cc'
        dwi_series = self.exam_validators[0].adc.get('high-b_cc')
        if not dwi_series or not os.path.isdir(dwi_series):
            dwi_type   = 'high-b'
            dwi_series = self.exam_validators[0].adc.get('high-b')
        if not dwi_series or not os.path.isdir(dwi_series):
            raise Exception('Could not find High-B DWI series')
        self._calculate_avg(dwi_series, dwi_type=dwi_type, skip=b0)

    def run_high_b_rfov(self, b0=2):
        self.logger.info('Processing High-B rFOV...')
        dwi_type   = 'high-b_rfov_cc'
        dwi_series = self.exam_validators[0].adc.get('high-b_rfov_cc')
        if not dwi_series or not os.path.isdir(dwi_series):
            dwi_type   = 'high-b_rfov'
            dwi_series = self.exam_validators[0].adc.get('high-b_rfov')
        if not dwi_series or not os.path.isdir(dwi_series):
            raise Exception('Could not find High-B rFOV DWI series')
        self._calculate_avg(dwi_series, dwi_type=dwi_type, skip=b0)

    def run_rfov(self, b0=2):
        self.logger.info('Processing rFOV...')
        dwi_series = self.exam_validators[0].adc.get('rfov')
        if not dwi_series or not os.path.isdir(dwi_series):
            raise Exception('Could not find rFOV DWI series')
        # self._calculate_geo_mean(dwi_series, dwi_type='rfov', skip=b0)
        self._calculate_adc(dwi_series, dwi_type='rfov', b0=b0)

    def run_rfov_brachy(self, b0=2):
        self.logger.info('Processing rFOV Brachy...')
        dwi_series = self.exam_validators[0].adc.get('rfov_brachy')
        if not dwi_series or not os.path.isdir(dwi_series):
            raise Exception('Could not find rFOV Brachy DWI series')
        # self._calculate_geo_mean(dwi_series, dwi_type='rfov_brachy', skip=b0)
        self._calculate_adc(dwi_series, dwi_type='rfov_brachy', b0=b0)

    def run_standard_diff(self):
        self.logger.info('Processing Standard Diffusion...')
        dwi_series = self.exam_validators[0].adc.get('std')
        if not dwi_series or not os.path.isdir(dwi_series):
            raise Exception('Could not find Standard Diffusion DWI series')
        self._run_diffu_dicom(dwi_series, dwi_type='std')

    def run_standard_diff_brachy(self):
        self.logger.info('Processing Standard Diffusion Brachy...')
        dwi_series = self.exam_validators[0].adc.get('std_brachy')
        if not dwi_series or not os.path.isdir(dwi_series):
            raise Exception('Could not find Standard Diffusion Brachy DWI series')
        self._run_diffu_dicom(dwi_series, dwi_type='std_brachy')

    def run(self):
        run_functions = {
                         'high-b':         self.run_high_b,
                         'high-b_cc':      self.run_high_b,
                         'high-b_rfov':    self.run_high_b_rfov,
                         'high-b_rfov_cc': self.run_high_b_rfov,
                         'rfov':           self.run_rfov,
                         'rfov_brachy':    self.run_rfov_brachy,
                         'std':            self.run_standard_diff,
                         'std_brachy':     self.run_standard_diff_brachy
                        }
        validator = self.exam_validators[0]
        for dwi_type, perform in validator.adc.iteritems():
            if perform:
                analysis = run_functions.get(dwi_type)
                if analysis:
                    try:
                        analysis()
                    except Exception, e:
                        self.logger.exception(e)


def run_adc(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run()


def run_high_b(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run_high_b()


def run_high_b_rfov(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run_high_b_rfov()


def run_rfov(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run_rfov()


def run_rfov_brachy(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run_rfov_brachy()


def run_standard_diff(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run_standard_diff()


def run_standard_diff_brachy(pc_num=None, pipe_logger=None):
    test = ProstateADCPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run_standard_diff_brachy()


def main():
    from pc_run_review import run_review

    logger           = lg.getLogger('run_adc')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()

    try:
        local_log = lg.FileHandler('run_adc.log')
        local_log.setFormatter(file_formatter)
        local_log.setLevel(lg.DEBUG)
        logger.addHandler(local_log)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        if args.run_high_b:
            run_high_b(pc_num=args.pc_num, pipe_logger=logger)
        elif args.run_high_b_rfov:
            run_high_b_rfov(pc_num=args.pc_num, pipe_logger=logger)
        elif args.run_rfov:
            run_rfov(pc_num=args.pc_num, pipe_logger=logger)
        elif args.run_rfov_brachy:
            run_rfov_brachy(pc_num=args.pc_num, pipe_logger=logger)
        elif args.run_standard_diff:
            run_standard_diff(pc_num=args.pc_num, pipe_logger=logger)
        elif args.run_standard_diff_brachy:
            run_standard_diff_brachy(pc_num=args.pc_num, pipe_logger=logger)
        else:
            run_adc(pc_num=args.pc_num, pipe_logger=logger)
        run_review(pc_num=args.pc_num, pipe_logger=logger)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Kicks off the ADC generation workflow, when run in a pc#### directory, created by
    pc_get_data. Will run a combination of: pc_diff_mean, pc_diff_adc, and/or diffu_dicom.
    Results are written to tensor. Auto-detects pc_number from directory.
    """)
    parser.add_argument('--pc_num',
                        dest    = "pc_num",
                        help    = "optional pc_number to use in map naming, i.e. pc1234")
    parser.add_argument('--high_b',
                        dest    = "run_high_b",
                        action  = "store_true",
                        default = False,
                        help    = "run only high_b")
    parser.add_argument('--high_b_rfov',
                        dest    = "run_high_b_rfov",
                        action  = "store_true",
                        default = False,
                        help    = "run only high_b_rfov")
    parser.add_argument('--rfov',
                        dest    = "run_rfov",
                        action  = "store_true",
                        default = False,
                        help    = "run only rfov")
    parser.add_argument('--rfov_brachy',
                        dest    = "run_rfov_brachy",
                        action  = "store_true",
                        default = False,
                        help    = "run only rfov_brachy")
    parser.add_argument('--standard_diff',
                        dest    = "run_standard_diff",
                        action  = "store_true",
                        default = False,
                        help    = "run only standard_diff")
    parser.add_argument('--standard_diff_brachy',
                        dest    = "run_standard_diff_brachy",
                        action  = "store_true",
                        default = False,
                        help    = "run only standard_diff_brachy")
    return parser.parse_args()

if __name__ == '__main__':
    main()
