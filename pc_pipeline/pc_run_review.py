#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import argparse    as ap
import subprocess  as sub
import shutil      as sh
import re
from   collections import namedtuple
from   pc_pipe     import ProstatePipeline
from   pc_utils    import mkdirs, change_permissions, ucsf_log_append
from   pc_data     import ProstateExam
from   dicom_info  import CompleteExamWalker
import logging     as lg


SVK_CONVERT = '/netopt/bin/local/svk_file_convert'


class ProstateReviewValidator(ProstateExam):
    def __init__(self, review_directory):
        super(ProstateReviewValidator, self).__init__(review_directory)
        self.series     = []
        self.review_dev = os.path.abspath(review_directory)
        series          = [os.path.join(self.review_dev, d) for d in os.listdir(review_directory)
                           if os.path.isdir(os.path.join(self.review_dev, d))]
        for d in series:
            info = CompleteExamWalker(d)
            if info.series:
                self.series.append(info.series[0])
        self.get_relevant_series()

    def get_relevant_series(self):
        self.review_series = []
        for idx, series in enumerate(self.series):
            self.review_series.append(series['/Path'])

    def validate(self):
        valid = True if self.review_series else False
        return valid


class ProstateReviewPipeline(ProstatePipeline):
    def __init__(self, pc_num=None, pipe_logger=None):
        super(ProstateReviewPipeline, self).__init__(pc_num=pc_num, pipe_logger=pipe_logger)

    def set_validator(self):
        self.validator = ProstateReviewValidator

    def validate(self):
        review_dir  = os.path.join(self.pc_directory, 'review_images')
        self.review = self.validator(review_dir)
        valid       = self.review.validate()
        if not valid:
            raise Exception('Could not find dicoms in review_images/')
        return valid

    def set_up_directories(self):
        review_images = os.path.join(self.pc_directory, 'ReviewImages')
        review_dev    = os.path.join(self.pc_directory, 'review_images')
        extra_dev     = os.path.join(review_images, 'extra')
        mkdirs(review_images)
        mkdirs(review_dev)
        mkdirs(extra_dev)
        change_permissions(review_images)
        change_permissions(review_dev)
        change_permissions(extra_dev)
        Directories      = namedtuple('Directories', ['ReviewImages', 'review_images', 'extra'])
        self.directories = Directories(review_images, review_dev, extra_dev)
        return self.directories

    def _convert_to_idf(self, series):
        self.logger.info('\tConverting %s', series)
        try:
            series_dcm  = self._get_image_series_input(series)
            output_base = os.path.basename(series)
            if '_cc' in output_base.lower() or re.search(r'S\d\d\d', output_base):
                output_base += '_div'
            output_root = os.path.join(self.directories.ReviewImages, output_base)
            command     = [SVK_CONVERT, '-i', series_dcm,
                                        '-o', output_root,
                                        '-t', '3']
            result      = sub.check_call(command)
        except Exception, e:
            self.logger.info('\tError converting %s to .idf', series)
            self.logger.info(e)
            change_permissions(self.directories.ReviewImages)
        else:
            change_permissions(self.directories.ReviewImages)
            return result

    def move_extras(self):
        extras = [r'ref', r'geo', r'dce_t5', r'pc[0-9]*_t1']
        for image in os.listdir(self.directories.ReviewImages):
            for desc in extras:
                if re.search(desc, image.lower()):
                    f = os.path.join(self.directories.ReviewImages, image)
                    dst_exists = os.path.join(self.directories.extra, image)
                    if os.path.exists(dst_exists):
                        os.remove(dst_exists)
                    try:
                        sh.move(f, self.directories.extra)
                    except Exception:
                        pass

    def run(self):
        self.logger.info('Converting review_images/ dicoms to .idfs in ReviewImages/')
        self.set_up_directories()
        for series in self.review.review_series:
            self._convert_to_idf(series)
        self.move_extras()


def run_review(pc_num=None, pipe_logger=None):
    test = ProstateReviewPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.set_up_directories()
    test.run()


def main():
    logger           = lg.getLogger('run_review')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()

    try:
        local_log = lg.FileHandler('run_review.log')
        local_log.setFormatter(file_formatter)
        local_log.setLevel(lg.DEBUG)
        logger.addHandler(local_log)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        run_review(pc_num=args.pc_num, pipe_logger=logger)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Converts all of the dicom series in review_images/, when run in a pc#### directory,
    created by pc_get_data. Will convert all series to .idf/.real, and put into ReviewImages/.
    """)
    parser.add_argument('--pc_num',
                        dest = "pc_num",
                        help = "optional pc_number to use in map naming, i.e. pc1234")
    return parser.parse_args()

if __name__ == '__main__':
    main()
