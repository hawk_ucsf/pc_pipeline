#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import shutil as sh
import errno
import re
import subprocess     as sub
import argparse       as ap
from   getpass        import getpass
from   fnmatch        import fnmatch
import logging        as lg
from   logging        import handlers
from   pc_utils       import ucsf_log_append, change_permissions, sym_link
from   pc_data        import ProstateExam, ProstateRaw, ValidationFail, AuthenticationFail


class ProstateSearcher(object):
    """Queries Jason's /data/dicom and /data/raw DB for relevant prostate exams,
    creates ProstateRaw and ProstateExam objects to check if all relevant data
    is present, and copies data to /data/pca1 or specified target directory.
    Creates pc-numbered processing directory and renames spectra."""

    LIST_SCANNER = '/netopt/share/bin/local/prostate/pc_list_scanner_data'
    SVK_CONVERT  = '/netopt/bin/local/svk_file_convert'

    def __init__(self, mrn=None, accession=None, password=None, logger=None):
        super(ProstateSearcher, self).__init__()
        if not logger:
            self.create_logger()
        else:
            self.logger = logger
        self.mrn         = str(mrn) if mrn else None
        self.accession   = str(accession) if accession else None
        self.exam_paths  = []
        self.pc_exams    = []
        self.raw_paths   = []
        self.ideal_paths = []
        self.pc_raw      = None
        self.valid       = True
        self.target      = os.getcwd()

    def create_logger(self):
        self.logger      = lg.getLogger('pc_get_data')
        log_stdout       = lg.StreamHandler(sys.stdout)
        stream_formatter = lg.Formatter('%(message)s')
        log_stdout.setFormatter(stream_formatter)
        log_stdout.setLevel(lg.INFO)
        self.logger.addHandler(log_stdout)
        self.logger.setLevel(lg.DEBUG)

    def set_kinit(self):
        """Check if TGT is expired, and prompt for password if renewal needed."""
        kinit = "kinit"
        proc  = sub.Popen([kinit, '-R'], stdin=sub.PIPE, stdout=sub.PIPE, stderr=sub.PIPE)
        out   = proc.communicate()
        if 'cannot read password' in out[1].lower() or 'expired' in out[1].lower():
            p     = getpass('Radauth password:')
            proc  = sub.Popen([kinit], stdin=sub.PIPE, stdout=sub.PIPE, stderr=sub.PIPE)
            proc.communicate(p)

    def get_data(self):
        """Call pc_list_scanner and parse output for exams and raw data."""
        try:
            self.set_kinit()
        except Exception, e:
            self.logger.info('Could not authenticate with raw DB: \n%s', e)
            raise AuthenticationFail()
        search_term    = self.accession if self.accession else self.mrn
        search_flag    = '-a' if self.accession else '-m'
        search_command = [ProstateSearcher.LIST_SCANNER, search_flag, search_term]
        try:
            output = sub.check_output(search_command)
        except sub.CalledProcessError, e:
            self.logger.info('Error checking exam database- please confirm accession number.')
            self.logger.debug(e)
            return False
        else:
            data_paths = [os.path.abspath(p) for p in output.split() if '/data' in p]
            for p in data_paths:
                if os.path.isdir(p):
                    self.exam_paths.append(p)
                elif os.path.isfile(p):
                    self.raw_paths.append(p)
                else:
                    continue
            # Find ideal files
            exam_numbers = []
            for path in self.exam_paths:
                exam = os.path.basename(path)
                if exam[0] == 'E':
                    exam_num = exam[1:]
                    if exam_num not in exam_numbers:
                        exam_numbers.append(exam_num)
            ideal_check_dirs = []
            # If no spectra were acquired, generate raw_paths from exam paths
            if not self.raw_paths:
                scanners = [path.split('/')[3] for path in self.exam_paths]
                raw_parn = '/data/raw_parn'
                ideal_check_dirs = [os.path.join(raw_parn, scanner)
                                    for scanner in scanners
                                    if os.path.isdir(os.path.join(raw_parn, scanner))]
            for path in self.raw_paths:
                raw_dir = os.path.dirname(path)
                if raw_dir not in ideal_check_dirs:
                    ideal_check_dirs.append(raw_dir)
            for path in ideal_check_dirs:
                for exam in exam_numbers:
                    file_list = [os.path.join(path, f) for f in os.listdir(path) if exam in f]
                    file_list = [f for f in file_list if 'ideal' in f]
                    self.ideal_paths.extend(file_list)
            return True

    def set_pc_number(self):
        """Generate a pc-number for processing."""
        pc_list  = sorted(os.listdir(self.target))
        pc_list  = [p for p in pc_list if re.match(r'^pc[0-9]*$', p)]
        if pc_list:
            last_pc  = pc_list[-1]
            last_num = int(last_pc[2:])
            new_num  = last_num + 1
        else:
            new_num = 1
        new_pc   = 'pc%04d' % new_num
        self.pc_number = new_pc

    def make_pc_dir(self, pc_number=None):
        """Create processing directory, incrementing pc numbers until successful."""
        limit = 5
        tries = 0
        while True:
            if not pc_number:
                self.set_pc_number()
            else:
                self.pc_number = pc_number
            self.pc_dir = os.path.join(self.target, self.pc_number)
            try:
                os.makedirs(self.pc_dir)
            except OSError, err:
                if err.errno == errno.EEXIST and not pc_number:
                    if tries < limit:
                        tries += 1
                        continue
                    else:
                        self.logger.info('Too many tries creating pc directory...')
                        self.pc_dir = None
                else:
                    self.logger.exception('Error creating pc_number:\n%s', err)
                    self.pc_dir = None
            finally:
                if self.pc_dir:
                    change_permissions(self.pc_dir)
                    return True
                else:
                    return False

    def parse_data(self):
        """Create ProstateExam and ProstateRaw objects out of results returned
        from exam/raw DB."""
        for exam in self.exam_paths:
            pc_exam = ProstateExam(exam)
            if pc_exam not in self.pc_exams:
                self.pc_exams.append(pc_exam)
        self.pc_raw = ProstateRaw(self.raw_paths, self.ideal_paths)

    def validate(self):
        """Runs exam and raw validation."""
        exam_fails = {}
        if not self.pc_exams:
            self.valid = False
            exam_fails.update({'exams': 'No exams found.'})
        else:
            for pc_exam in self.pc_exams:
                valid = pc_exam.validate()
                if not valid:
                    self.valid = valid
                else:
                    break
            if not self.valid:
                for exam in self.pc_exams:
                    if not exam_fails:
                        exam_fails.update(exam.failures)
                    else:
                        # Compare with previous exam's failures
                        if not exam.failures:
                            exam.failures = {}
                        for key, val in exam.failures.iteritems():
                            if key in exam_fails:
                                # Get the overlap of exam failures
                                fails = [d for d in val if d in exam_fails[key]]
                                if not fails:
                                    del exam_fails[key]
                                else:
                                    exam_fails[key] = fails
                        keys_to_remove = []
                        for key, val in exam_fails.iteritems():
                            if key not in exam.failures:
                                keys_to_remove.append(key)
                        for key in keys_to_remove:
                            del exam_fails[key]
                if not exam_fails:
                    self.valid = True
        valid = self.pc_raw.validate()
        if not valid:
            self.valid = valid
        if not self.valid:
            raise ValidationFail(exam_dict=exam_fails, raw_dict=self.pc_raw.failures)

    def copy_dicoms(self):
        """Copies exam to processing directory"""
        transferred = []
        for exam in self.exam_paths:
            self.logger.info('Copying %s to %s...', exam, self.pc_dir)
            try:
                exam_dir    = os.path.join(self.pc_dir, os.path.split(exam)[1])
                try:
                    series_list = sorted(os.listdir(exam), key=int)
                except ValueError, e:
                    series_list = sorted(os.listdir(exam))
                num_series  = len(series_list)
                for idx, series in enumerate(series_list):
                    dicom_series = os.path.join(exam, series)
                    series_dest  = os.path.join(exam_dir, series)
                    self.logger.info('(%2d of %d) Copying Series %s...', idx+1, num_series, series)
                    try:
                        sh.copytree(dicom_series, series_dest)
                    except Exception, e:
                        self.logger.exception('Could not copy %s to %s', dicom_series, series_dest)
                    else:
                        change_permissions(series_dest)
            except Exception, e:
                self.logger.exception('Could not copy %s to %s, error:\n%s', exam, self.pc_dir, e)
                continue
            else:
                self.logger.info('%s copied to %s', exam, self.pc_dir)
                change_permissions(exam_dir)
                transferred.append(exam)
        return transferred

    def copy_raw(self):
        """Copy raw data to processing directory"""
        transferred = []
        self.logger.info('Copying raw data to %s...', self.pc_dir)
        for idx, raw in enumerate(self.raw_paths):
            self.logger.info('(%d of %d) Copying %s...', idx+1, len(self.raw_paths), raw)
            try:
                sh.copy(raw, self.pc_dir)
                change_permissions(os.path.join(self.pc_dir, os.path.basename(raw)))
            except Exception, e:
                self.logger.exception('Could not copy %s to %s, error:\n%s', raw, self.pc_dir, e)
            else:
                self.logger.info('%s copied to %s', raw, self.pc_dir)
                transferred.append(raw)
        new_names = [self.pc_number + ext for ext in ['', 'w', 'sw']]
        for idx, raw in enumerate([self.pc_raw.full, self.pc_raw.w, self.pc_raw.sw]):
            if raw:
                try:
                    old_name = os.path.split(raw)[1]
                    new_name = new_names[idx]
                    old_path = os.path.join(self.pc_dir, old_name)
                    new_path = os.path.join(self.pc_dir, new_name)
                    os.rename(old_path, new_path)
                except Exception, e:
                    pass
        for idx, ideal in enumerate(self.ideal_paths):
            self.logger.info('(%d of %d) Copying %s...', idx+1, len(self.ideal_paths), ideal)
            try:
                sh.copy(ideal, self.pc_dir)
                change_permissions(os.path.join(self.pc_dir, os.path.basename(ideal)))
            except Exception, e:
                self.logger.exception('Could not copy %s to %s, error:\n%s', ideal, self.pc_dir, e)
            else:
                self.logger.info('%s copied to %s', ideal, self.pc_dir)
                transferred.append(ideal)
        return transferred

    def convert_to_idf(self):
        """Converts coil corrected reformats to .idf"""
        self.logger.info('Starting .idf/.int2 conversion...')
        converted      = []
        image_dir_path = os.path.join(self.pc_dir, 'images_test')
        try:
            os.makedirs(image_dir_path)
        except OSError, e:
            if e.errno == errno.EEXIST:
                pass
            else:
                self.logger.exception('Could not create %s, error:\n%s', image_dir_path, e)
                return None
        except Exception, e:
            self.logger.exception('Could not create images directory, error:\n%s', e)
            return None
        finally:
            name_base = os.path.join(image_dir_path, self.pc_number)
            for exam in self.pc_exams:
                for path in exam.review_list:
                    series_num  = str(os.path.split(path)[1])
                    exam_num    = 'E' + str(exam.exam['Exam'])
                    series_name = exam_num + 'S' + series_num
                    pc_path     = os.path.join(self.pc_dir, exam_num, series_num)
                    file_name   = '_'.join([name_base, series_name, 'div'])
                    try:
                        first_dicom = [d for d in sorted(os.listdir(pc_path)) if '.dcm' in d.lower()][0]
                    except Exception, e:
                        self.logger.info('No dicoms found in %s.', pc_path)
                        continue
                    first_dicom = os.path.join(pc_path, first_dicom)
                    command     = [ProstateSearcher.SVK_CONVERT,
                                   '-i',
                                   first_dicom,
                                   '-o',
                                   file_name,
                                   '-t',
                                   '3']
                    try:
                        self.logger.info('Converting %s...', pc_path)
                        self.logger.debug(command)
                        res = sub.check_call(command)
                    except sub.CalledProcessError, e:
                        self.logger.info('Error converting %s to .idf', pc_path)
                        self.logger.debug(e)
                    else:
                        self.logger.info('%s converted to %s.idf', pc_path, file_name)
                        converted.append(pc_path)
                # Change StudyID from accession to MRN
                idf_list = [os.path.join(image_dir_path, idf) for idf in os.listdir(image_dir_path)
                            if idf[-4:] == '.idf']
                for idf in idf_list:
                    try:
                        with open(idf, 'r') as f:
                            old_hdr = f.read()
                        new_hdr = old_hdr.replace(str(exam.exam['Accession']), str(exam.exam['Patient']['MRN']))
                        with open(idf, 'w') as f:
                            f.write(new_hdr)
                    except Exception, e:
                        self.logger.info('Error replacing StudyID with MRN in %s', idf)
                        self.logger.debug(e)
                        continue
            change_permissions(image_dir_path)
        return converted

    def set_up_review_images(self):
        def is_axial_t1(description):
            if fnmatch(description.lower(), '*ax*t1*'):
                return True

        def is_axial_t2(description):
            if fnmatch(description.lower(), '*ax*t2*') and 'cube' not in description.lower():
                return True

        def is_axial_cube(description):
            if fnmatch(description.lower(), 'ax*cube*t2*'):
                return True

        self.logger.info('Setting up review_images/...')
        image_dir_path = os.path.join(self.pc_dir, 'review_images')
        review         = []
        try:
            os.makedirs(image_dir_path)
        except OSError, e:
            if e.errno == errno.EEXIST:
                pass
            else:
                self.logger.exception('Could not create %s, error:\n%s', image_dir_path, e)
                return None
        except Exception, e:
            self.logger.exception('Could not create review images directory, error:\n%s', e)
            return None
        else:
            for exam in self.pc_exams:
                for path, desc in exam.review_list:
                    if is_axial_cube(desc):
                        description = 'Ax_Cube_T2'
                    elif is_axial_t2(desc):
                        description = 'Ax_T2'
                    elif is_axial_t1(desc):
                        description = 'Ax_T1'
                    else:
                        description = '_'.join(desc.split())
                    series_num  = str(os.path.split(path)[1])
                    exam_num    = 'E' + str(exam.exam['Exam'])
                    series_name = self.pc_number + '_S' + series_num + '_' + description
                    print series_name
                    pc_path     = os.path.join('..', exam_num, series_num)
                    sym_link(pc_path, image_dir_path, link_name=series_name)
                    review.append(series_name)
            return review

    def summarize_results(self, exams=[], raws=[], reviews=[]):
        self.logger.info('\n')
        self.logger.info("=========================")
        self.logger.info("Exams transferred to %s:", self.pc_dir)
        self.logger.info("=========================")
        for exam in exams:
            self.logger.info("\t- %s", exam)
        self.logger.info("=========================")
        self.logger.info("Raw files transferred to %s:", self.pc_dir)
        self.logger.info("=========================")
        for raw in raws:
            self.logger.info("\t- %s", raw)
        self.logger.info("=========================")
        self.logger.info("Review images in %s:", os.path.join(self.pc_dir, 'review_images'))
        self.logger.info("=========================")
        for image in reviews:
            self.logger.info("\t- %s", image)


def get_data(args, logger):
    logger.debug('User: %s, Command: %s', os.getlogin(), sys.argv)
    pc_search = ProstateSearcher(accession=args.accession, logger=logger)
    if args.target:
        target = os.path.abspath(args.target)
        if os.path.isdir(target):
            pc_search.target = target
        else:
            logger.info("%s is not a directory, or doesn't exist.", target)
            return
    pc_search.logger.info("Searching for exams...")
    if pc_search.get_data():
        pc_search.logger.info("Parsing exams...")
        pc_search.parse_data()
        pc_search.logger.info("Exams found:")
        for exam in pc_search.pc_exams:
            if args.verbose:
                pc_search.logger.info(exam.summarize())
            else:
                pc_search.logger.info('\t' + exam.directory)
                pc_search.logger.debug(exam.summarize())
        pc_search.logger.info("Raw spectra found:")
        for raw in pc_search.raw_paths + pc_search.ideal_paths:
            pc_search.logger.info('\t' + raw)
        try:
            pc_search.validate()
        except ValidationFail as VF:
            pc_search.logger.info('Validation failed for %d:\n%s', args.accession, VF)
        finally:
            if not args.dump:
                if not pc_search.valid and not args.force:
                    print "\n** Use --force option to override validation and copy data."
                else:
                    if args.pc_num:
                        created = pc_search.make_pc_dir(args.pc_num)
                    else:
                        created = pc_search.make_pc_dir()
                    if created:
                        try:
                            local_logger   = lg.getLogger('local_log')
                            local_log      = lg.FileHandler(os.path.join(pc_search.pc_dir,
                                                                         "get_data.log"))
                            file_formatter = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
                            local_log.setFormatter(file_formatter)
                            local_log.setLevel(lg.DEBUG)
                            local_logger.setLevel(lg.DEBUG)
                            local_logger.addHandler(local_log)
                            pc_search.logger.addHandler(local_log)
                            local_logger.debug('User: %s, Command: %s', os.getlogin(), sys.argv)
                            change_permissions(os.path.join(pc_search.pc_dir, "get_data.log"))
                            for exam in pc_search.pc_exams:
                                if args.verbose:
                                    local_logger.debug(exam)
                                else:
                                    local_logger.debug(exam.summarize())
                            for raw in pc_search.raw_paths:
                                local_logger.debug('\t' + raw)
                        except Exception, e:
                            pc_search.logger.exception('Problem creating local log:\n%s', e)
                        transferred_exams    = pc_search.copy_dicoms()
                        transferred_raw      = pc_search.copy_raw()
                        review_images        = pc_search.set_up_review_images()
                        pc_search.summarize_results(transferred_exams,
                                                    transferred_raw,
                                                    review_images)
                        return pc_search.pc_dir


def main():
    from pc_run_dce    import run_coil as run_dce_coil
    from pc_run_review import run_review

    logger           = lg.getLogger('pc_get_data')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()

    try:
        log_file = handlers.RotatingFileHandler("get_data.log",
                                                maxBytes    = 1024*1024*10,
                                                backupCount = 2)
        log_file.setFormatter(file_formatter)
        log_file.setLevel(lg.DEBUG)
        logger.addHandler(log_file)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        pc_directory = get_data(args, logger)
        if not args.dump:
            os.chdir(pc_directory)
            try:
                run_dce_coil(pc_num=args.pc_num, pipe_logger=logger)
            except Exception, e:
                logger.exception(e)
            run_review(pc_num=args.pc_num, pipe_logger=logger)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Queries the dicom and raw data directories for a prostate study, using the
    provided accession number. If the study is found, will validate the exam for
    relevant series and raw files, create a pc-numbered directory in the current
    directory,  and transfer over the images + spectra (renaming the spectra).
    Coil-corrected anatomic images with be converted to .idf/.int2.
    """)
    requiredNamed = parser.add_argument_group("required named arguments")
    requiredNamed.add_argument('-a', '--accession',
                               dest     = "accession",
                               type     = int,
                               required = True,
                               help     = "accession number of study")
    parser.add_argument('-d', '--dump',
                        action  = "store_true",
                        default = False,
                        help    = "summarize found dicoms and spectra, but don't transfer")
    parser.add_argument('-v', '--verbose',
                        action  = "store_true",
                        default = False,
                        help    = "display exam and series info in detail")
    parser.add_argument('-f', '--force',
                        action  = "store_true",
                        default = False,
                        help    = "transfer over found data, even if validation fails")
    parser.add_argument('-t', '--target',
                        dest = "target",
                        help = "directory to copy data to, instead of /data/pca1")
    parser.add_argument('--pc_num',
                        dest = "pc_num",
                        help = "optional pc_number to use in map naming, i.e. pc1234")
    return parser.parse_args()

if __name__ == '__main__':
    main()
