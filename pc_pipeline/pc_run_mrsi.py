#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import shutil      as sh
import argparse    as ap
import subprocess  as sub
from   fnmatch     import fnmatch
from   collections import namedtuple
from   dicom_info  import dict_format
from   pc_pipe     import ProstatePipeline
from   pc_data     import ProstateExam
from   pc_run_dce  import int_filter
from   pc_utils    import mkdirs, change_permissions, ucsf_log_append, natural_key
import logging     as lg


SVK_GEPFILE_ANON   = '/netopt/bin/local/svk_gepfile_anon'
AUTOPHASE          = '/netopt/bin/local/prostate/autophase'
SVK_GEPFILE_READER = '/netopt/bin/local/svk_gepfile_reader'
PROCESS_SPEC_V6    = '/netopt/bin/local/process_spec_v6'
PEAK_PATH          = '/netopt/share/lib/local/parameters/prostate'
PEAK_FILE          = 'generic.2000Hz.v5_ucsf_3T.peak'
PROCESS_P          = '/netopt/share/bin/local/process_p'


class ProstateMRSIValidator(ProstateExam):
    """
    A simple validator, based off of pc_get_data's ProstateExam. Looks for
    full spectra, and either water or water suppressed spectra, and stores the
    paths for ProstateMRSIPipeline to use.
    """

    def __init__(self, pc_dir, pc_num=None):
        self.pc_dir = os.path.abspath(pc_dir)
        if not pc_num:
            self.pc_num = os.path.basename(self.pc_dir)
        else:
            self.pc_num = pc_num
        super(ProstateMRSIValidator, self).__init__(pc_dir)
        self.valid_spectra = False
        self.full          = None
        self.s             = None
        self.sw            = None

    def get_relevant_series(self):
        files = os.listdir(self.pc_dir)
        self.spectra = [os.path.join(self.pc_dir, f) for f in files
                        if fnmatch(f, self.pc_num + '*') and os.path.isfile(os.path.join(self.pc_dir, f))]

    def validate(self):
        full = False
        part = False
        for s in self.spectra:
            if fnmatch(s, os.path.join(self.pc_dir, self.pc_num)):
                full      = True
                self.full = s
            elif fnmatch(s, os.path.join(self.pc_dir, self.pc_num + 's')):
                part   = True
                self.s = s
            elif fnmatch(s, os.path.join(self.pc_dir, self.pc_num + 'sw')):
                part    = True
                self.sw = s

        valid = True if full else False
        if not valid:
            self.failures = {}
            if not full:
                self.failures['spectra'] = [('full', False)]
            if not part:
                self.failures['spectra'] = [('part', False)]
        return valid


class MRSIValidationFail(Exception):
    """
    Custom validation failure exception, based off of pc_get_data's ValidationFail.
    """

    exam_msgs = {
                 'spectra': {
                             'full': "Full spectrum file is missing.",
                             'part': "s/sw spectra missing - needs one.",
                            }
                }

    def __init__(self, spectra_dict={}):
        self.spectra_messages = []
        self.get_messages(self.spectra_messages, spectra_dict)

    def get_messages(self, message_list, message_dict):
        for base_key, failures in message_dict.iteritems():
            for series, status in failures:
                message = MRSIValidationFail.exam_msgs[base_key][series]
                message_list.append(message)

    @dict_format
    def __str__(self):
        return {
                'Spectra Failures':  sorted(['* ' + m for m in self.spectra_messages])
               }


class ProstateMRSIPipeline(ProstatePipeline):
    """
    Implements pc_run_pipe's ProstatePipeline, with ProstateMRSIValidator as its
    validator. Runs through these steps:
        - Creates spectra directory
        - Modify's PFile using svk_gepfile_anon
        - Converts PFile using svk_gepfile_reader
        - Reconstructs spectra using prostate_epsi_phase_correct_hack,
          set_fb_pars_prostate.x, and process_spec_v6
        - Quantifies spectra using run_csi_image_epsi.x, autophase, and process_p
    """
    def __init__(self, pc_num=None, pipe_logger=None):
        super(ProstateMRSIPipeline, self).__init__(pc_num=pc_num, pipe_logger=pipe_logger)

    def set_validator(self):
        self.validator = ProstateMRSIValidator

    def validate(self):
        self.logger.info('Validating spectra...')
        self.spectra = self.validator(self.pc_directory, self.pc_num)
        self.valid   = self.spectra.validate()
        if not self.valid:
            raise MRSIValidationFail(spectra_dict=self.spectra.failures)

    def set_up_directories(self):
        spectra_dir = os.path.join(self.pc_directory, 'spectra')
        mkdirs(spectra_dir)
        change_permissions(spectra_dir)
        sh.copy('/netopt/share/bin/local/prostate/prostate_epsi_phase_correct_hack.m',
                spectra_dir)
        sh.copy('/netopt/share/bin/local/prostate/set_fb_pars_prostate.x',
                spectra_dir)
        sh.copy('/netopt/share/bin/local/prostate/run_csi_image_epsi.x',
                 spectra_dir)
        sh.copy(os.path.join(PEAK_PATH, PEAK_FILE),
                os.path.join(spectra_dir, self.pc_num + '.peak'))
        Directories      = namedtuple('Directories', ['spectra_dir'])
        self.directories = Directories(spectra_dir)
        return self.directories

    def run_modify(self):
        self.logger.info('Modifying PFile...')
        try:
            spectra = self.spectra.full
            field   = 'rhi.psdname'
            value   = 'prose_prostate_ucsf'
            command = [SVK_GEPFILE_ANON, '-i', spectra,
                                         '-f', field,
                                         '-v', value]
            result  = sub.check_call(command)
        except Exception, e:
            self.logger.info('Error modifying PFile...')
            raise e
        else:
            return result

    def run_convert_pfile(self):
        self.logger.info('Converting PFile...')
        try:
            p_file     = self.spectra.full
            output     = os.path.join(self.directories.spectra_dir, self.pc_num)
            epsi_type  = '3'
            epsi_axis  = '1'
            epsi_lobes = '431'
            epsi_skip  = '3'
            epsi_first = '1'
            command    = [SVK_GEPFILE_READER, '-i', p_file,
                                              '-o', output,
                                              '-t', '2',
                                              '--epsi_type', epsi_type,
                                              '--epsi_axis', epsi_axis,
                                              '--epsi_lobes', epsi_lobes,
                                              '--epsi_skip', epsi_skip,
                                              '--epsi_first', epsi_first]
            result     = sub.check_call(command)
        except Exception, e:
            self.logger.info('Error converting PFile...')
            change_permissions(self.directories.spectra_dir)
            raise e
        else:
            change_permissions(self.directories.spectra_dir)
            return result

    def run_reconstruction(self):
        self.logger.info('Reconstructing PFile...')
        try:
            ddf_list = sorted([f for f in os.listdir(self.directories.spectra_dir)
                               if fnmatch(f, self.pc_num + '*.ddf')])
            ddf_list = [f for f in ddf_list if int_filter(os.path.splitext(f)[0][-1])]
            ddf_list = sorted(ddf_list, key=natural_key)
            ddf      = ddf_list[-1]
            ddf      = os.path.join(self.directories.spectra_dir, ddf)
            remove   = [os.path.join(self.directories.spectra_dir, f) for f in ddf_list[:-1]]
            for f in remove:
                base = os.path.splitext(f)[0]
                os.remove(f)
                os.remove(base + '.cmplx')
            if not os.path.isfile(ddf):
                raise Exception('Could not find %s', ddf)
        except Exception, e:
            raise Exception('Could not find .ddf')

        try:
            os.chdir(self.directories.spectra_dir)
            root    = os.path.splitext(ddf)[0]
            code    = "prostate_epsi_phase_correct_hack('" + root + "'); exit"
            command = ['matlab', '-nojvm', '-r', code]
            result  = sub.check_call(command)

            set_params = os.path.join(self.directories.spectra_dir,
                                      'set_fb_pars_prostate.x')
            command    = [set_params, os.path.basename(root), self.pc_num + '_fb']
            result     = sub.check_call(command)

            params        = self.pc_num + '_fb.par'
            complex_input = root + '.cmplx'
            command       = [PROCESS_SPEC_V6, '-p', params,
                                              '-n', '19',
                                              '-m', '0.0',
                                              '-a', '2',
                                              os.path.basename(complex_input)]
            result        = sub.check_call(command)
        except Exception, e:
            self.logger.info('Error Reconstructing PFile...')
            os.chdir(self.pc_directory)
            change_permissions(self.directories.spectra_dir)
            raise e
        else:
            os.chdir(self.pc_directory)
            change_permissions(self.directories.spectra_dir)
            return result

    def run_quantification(self):
        self.logger.info('Quantifying spectra...')
        try:
            cmplx_list = sorted([f for f in os.listdir(self.directories.spectra_dir)
                                 if fnmatch(f, self.pc_num + '*_phased.cmplx')])
            cmplx      = cmplx_list[0]
            cmplx      = os.path.join(self.directories.spectra_dir, cmplx)
        except Exception, e:
            raise Exception('Could not find phased complex file')

        try:
            os.chdir(self.directories.spectra_dir)
            self.logger.info('\tRunning CSI image epsi...')
            phased   = os.path.splitext(os.path.basename(cmplx))[0]
            root     = phased.split('_phased')[0]
            coil_num = root.split('_')[1]
            met_root = root + '_cor_met'
            met_dir  = met_root
            mkdirs(met_dir)
            change_permissions(met_dir)
            peak        = self.pc_num + '.peak'
            phased_peak = root + '.peak'
            sh.copy(peak, phased_peak)
            run_csi = './run_csi_image_epsi.x'
            command = [run_csi, os.path.basename(cmplx),
                                os.path.join(met_dir, phased),
                                peak,
                                root + '_cor.cmplx']
            result  = sub.check_call(command)

            self.logger.info('\tRunning autophase...')
            cor_sum = os.path.join(self.directories.spectra_dir, root + '_cor_sum')
            cor_pft = os.path.join(self.directories.spectra_dir, root + '_cor_pft')
            command = [AUTOPHASE, cor_sum, cor_pft]
            self.logger.info(command)
            result  = sub.check_call(command)

            cor_rephased = root + '_cor_rephased'
            sh.copy(cor_pft + '.ddf', cor_rephased + '.ddf')
            sh.copy(cor_pft + '.cmplx', cor_rephased + '.cmplx')

            self.logger.info('\tRunning process_p...')
            command = [PROCESS_P, '-c', coil_num,
                                  '-i', cor_rephased + '.cmplx',
                                  self.pc_num]
            result  = sub.check_call(command)
        except Exception, e:
            os.chdir(self.pc_directory)
            self.logger.info('Error Quantifying PFile...')
            change_permissions(self.directories.spectra_dir)
            raise e
        else:
            os.chdir(self.pc_directory)
            change_permissions(self.directories.spectra_dir)
            return result

    def run(self):
        self.logger.info('Running MRSI workflow...')
        self.set_up_directories()
        self.run_modify()
        self.run_convert_pfile()
        self.run_reconstruction()
        self.run_quantification()


def run_mrsi(pc_num=None, pipe_logger=None):
    test = ProstateMRSIPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    test.validate()
    test.run()


def main():
    logger           = lg.getLogger('run_mrsi')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()

    try:
        local_log = lg.FileHandler('run_mrsi.log')
        local_log.setFormatter(file_formatter)
        local_log.setLevel(lg.DEBUG)
        logger.addHandler(local_log)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        run_mrsi(pc_num=args.pc_num, pipe_logger=logger)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Kicks off the MRSI quantification workflow, when run in a pc#### directory, created by
    pc_get_data. Will run: svk_gepfile_anon, prostate_epsi_phase_correct_hack, svk_gepfile_reader
    set_fb_pars_prostate.x, process_spec_v6, run_csi_image_epsi.x, autophase, and process_p.
    Results are written to spectra/. Auto-detects pc_number from directory.
    """)
    parser.add_argument('--pc_num',
                        dest    = "pc_num",
                        help    = "optional pc_number to use in map naming, i.e. pc1234")
    return parser.parse_args()

if __name__ == '__main__':
    main()
