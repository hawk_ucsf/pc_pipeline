import os
from   fnmatch        import fnmatch
from   collections    import OrderedDict
from   dicom_info     import CompleteExamWalker, dict_format


class ProstateExam(CompleteExamWalker):
    """Exam/Series information for a prostate exam. Contains methods to validate
    series data for processing."""
    def __init__(self, directory):
        super(ProstateExam, self).__init__(directory)
        self.valid_dce      = None
        self.valid_dwi      = None
        self.valid_ideal    = None
        self.valid_anatomic = None
        self.failures       = {}
        self.relevant_dict  = {
                               'dce': ['*dce*', '*pregd*'],
                               'dwi': {
                                       'rfov':           [
                                                          '*ax dwi rfov b=600*',
                                                          '*b=600 ax dwi rfov*',
                                                          '*ax dwi b=600*'
                                                         ],
                                       'rfov brachy':    [
                                                          '*pt with seeds*b=600 ax dwi rfov*'
                                                         ],
                                       'high-b':         [
                                                          '*epi diffu-b=1350*',
                                                          '*ax dwi b=1350*'
                                                         ],
                                       'high-b cc':      [
                                                          '*epi diffu-b=1350*',
                                                          '*ax dwi b=1350*'
                                                         ],
                                       'high-b rfov':    [
                                                          '*b=1350 ax dwi rfov*',
                                                          '*ax dwi rfov b=1350*'
                                                         ],
                                       'high-b rfov cc': [
                                                          '*b=1350 ax dwi rfov*',
                                                          '*ax dwi rfov b=1350*'
                                                         ],
                                       'std':            [
                                                          'epi diffusion with asset',
                                                          'epi diffusion',
                                                          '*epi diffu*b=600*'
                                                         ],
                                       'std brachy':     [
                                                          'epi diffusion*brachy'
                                                         ],
                                      },
                               'ideal': ['ideal'],
                               'anatomic': {
                                            'axial t1':         [
                                                                 '*ax*t1*',
                                                                 '*ax*t1 3d'
                                                                ],
                                            'axial t2':         [
                                                                 '*ax*t2*',
                                                                ],
                                            'coronal t2':       [
                                                                 'cor*t2*'
                                                                ],
                                            'sagittal t2':      [
                                                                 'sag*t2*'
                                                                ],
                                            'sagittal cube':    [
                                                                 'sag*ref*',
                                                                 'cube*sag*ref'
                                                                ],
                                            'coronal cube':     [
                                                                 'cor*ref*',
                                                                 'cube*cor*ref*'
                                                                ],
                                            'axial cube':       [
                                                                 'ax*cube*t2*'
                                                                ],
                                            'sagittal cube cc': [
                                                                 'sag cc reformat'
                                                                ],
                                            'coronal cube cc':  [
                                                                 'cor cc reformat'
                                                                ]
                                           }
                              }
        self.get_relevant_series()

    @dict_format
    def summarize(self):
        """Returns a formatted string summary of exam/series info."""
        info   = {
                  'Exam':   self.exam,
                  'Series': [{
                              s['Series']: {
                                            '/Path':       s['/Path'],
                                            'Description': s['Description'],
                                            'Images':      s['Images'],
                                            'Transferred': s['Transferred']
                                           }
                             }
                             for s in self.series]
                 }
        return info

    def is_dce(self, series):
        for desc in self.relevant_dict['dce']:
            if fnmatch(series['Description'].lower(), desc):
                return True

    def is_dwi(self, series):
        for desc_list in self.relevant_dict['dwi'].itervalues():
            for desc in desc_list:
                if fnmatch(series['Description'].lower(), desc):
                    return True

    def is_ideal(self, series):
        for desc in self.relevant_dict['ideal']:
            if desc in series['Description'].lower():
                return True

    def is_anatomic(self, series):
        for desc_list in self.relevant_dict['anatomic'].itervalues():
            for desc in desc_list:
                if fnmatch(series['Description'].lower(), desc):
                    return True

    def get_relevant_series(self):
        """Builds a list of necessary series, from parsed CompleteExamWalker
        exam info, based off of series descriptions."""
        self.dce_series      = []
        self.dwi_series      = []
        self.ideal_series    = []
        self.anatomic_series = []
        self.review_list     = []
        for idx, series in enumerate(self.series):
            series_tup = idx, series['Series'], series['Description']
            if self.is_dce(series):
                self.dce_series.append(series_tup)
            elif self.is_dwi(series):
                self.dwi_series.append(series_tup)
            elif self.is_ideal(series):
                self.ideal_series.append(series_tup)
            elif self.is_anatomic(series):
                self.anatomic_series.append(series_tup)
        new_anatomic         = self.remove_repeated_reformats(self.anatomic_series)
        self.anatomic_series = new_anatomic

    @staticmethod
    def remove_repeated_reformats(relevant_series):
        """Takes the higher series numbered series on repeated series descriptions"""
        reformat_list = [s for s in relevant_series if len(str(s[1])) >= 3]
        reformat_dict = OrderedDict()
        for s in reformat_list:
            old = reformat_dict.get(s[2])
            if not old:
                reformat_dict[s[2]] = s
            else:
                if int(s[1]) > old[1]:
                    reformat_dict[s[2]] = s
        return [val for val in reformat_dict.itervalues()]

    def validate(self):
        """Runs all of the validation steps, and logs failures."""
        check = [self.check_dce(),
                 self.check_dwi(),
                 self.check_ideal(),
                 self.check_anatomic()]
        valid = False if False in check else True
        if not valid:
            fails = [idx for (idx, val) in enumerate(check) if not val]
            self.failures = {}
            for idx in fails:
                if idx == 0:
                    self.failures['dce'] = [(key, val) for (key, val) in
                                            self.valid_dce.iteritems()
                                            if not val]
                elif idx == 1:
                    self.failures['dwi'] = [(key, val) for (key, val) in
                                            self.valid_dwi.iteritems()
                                            if not val]
                elif idx == 2:
                    self.failures['ideal'] = [(key, val) for (key, val) in
                                               self.valid_ideal.iteritems()
                                               if not val]
                elif idx == 3:
                    self.failures['anatomic'] = [(key, val) for (key, val) in
                                                 self.valid_anatomic.iteritems()
                                                 if not val]
        return valid

    def check_dce(self):
        """Uses 'Images' dicom tag to determine of DCE and preGd series are
        completely transferred."""
        res = {'dce_transferred': False, 'pre_transferred': False}
        for series_tup in self.dce_series:
            idx, num, desc = series_tup
            series         = self.series[idx]
            if 'pet' in self.exam['Station']['Model'].lower():
                if 'dce' in desc.lower():
                    res['dce_transferred'] = True
                elif 'pregd' in desc.lower():
                    res['pre_transferred'] = True
            elif len(str(num)) <= 2:
                if 'dce' in desc.lower():
                    res['dce_transferred'] = res['dce_transferred'] \
                                             if res['dce_transferred'] \
                                             else series['Transferred']
                elif 'pregd' in desc.lower():
                    res['pre_transferred'] = res['pre_transferred'] \
                                             if res['pre_transferred'] \
                                             else series['Transferred']
        self.valid_dce = res
        check          = [val for (key, val) in res.iteritems()]
        valid          = False if False in check else True
        return valid

    def check_dwi(self):
        """Uses 'Images' dicom tag and series number to determine if high
        b-value DWI series and it's coil corrected reformation are completely
        transferred."""
        res = {'transferred': False, 'corrected': False}
        for series_tup in self.dwi_series:
            idx, num, desc = series_tup
            series         = self.series[idx]
            if len(str(num)) <= 2:
                res['transferred'] = res['transferred'] if res['transferred'] \
                                     else series['Transferred']
            else:
                res['corrected'] = res['corrected'] if res['corrected'] \
                                         else series['Transferred']
        self.valid_dwi = res
        check          = [val for (key, val) in res.iteritems()]
        valid          = False if False in check else True
        return valid

    def check_ideal(self):
        """Uses series number and description to determine if Ideal series
        and it's coil corrected reformation are present."""
        res        = {'transferred': False, 'corrected': False}
        series_num = 0
        for series_tup in self.ideal_series:
            idx, num, desc = series_tup
            if len(str(num)) <= 2:
                res['transferred'] = True
                series_num = num
            else:
                res['corrected'] = (num == series_num * 100)
                if res['corrected']:
                    break
        self.valid_ideal = res
        check            = [val for (key, val) in res.iteritems()]
        valid            = False if False in check else True
        return valid

    def check_anatomic(self):
        """Uses series numbers and descriptions to determine if anatomic series
        and their reformations are present."""
        def is_axial_t1(description):
            for pattern in self.relevant_dict['anatomic']['axial t1']:
                if fnmatch(description.lower(), pattern):
                    return True

        def is_axial_t2(description):
            for pattern in self.relevant_dict['anatomic']['axial t2']:
                if fnmatch(description.lower(), pattern) and 'cube' not in description.lower():
                    return True

        def is_axial_cube(description):
            for pattern in self.relevant_dict['anatomic']['axial cube']:
                if fnmatch(description.lower(), pattern):
                    return True

        def is_coronal_reformat(description):
            for pattern in self.relevant_dict['anatomic']['coronal cube']:
                if fnmatch(description.lower(), pattern):
                    return True

        def is_sagittal_reformat(description):
            for pattern in self.relevant_dict['anatomic']['sagittal cube']:
                if fnmatch(description.lower(), pattern):
                    return True

        res      = {
                    't1_reformat':   False,
                    't2_reformat':   False,
                    'cube_reformat': False,
                    'cor_reformat':  False,
                    'sag_reformat':  False
                   }
        cube_num = 0
        t1_num   = 0
        t2_num   = 0
        for series_tup in self.anatomic_series:
            idx, num, desc = series_tup
            series         = self.series[idx]
            if len(str(num)) <= 2:
                if is_axial_t1(desc):
                    t1_num = num
                elif is_axial_t2(desc):
                    t2_num = num
                elif is_axial_cube(desc):
                    cube_num = num
            else:
                if num == t1_num * 100 or is_axial_t1(desc):
                    res['t1_reformat'] = True
                    self.review_list.append((series['/Path'], series['Description']))
                elif num == t2_num * 100 or is_axial_t2(desc):
                    res['t2_reformat'] = True
                    self.review_list.append((series['/Path'], series['Description']))
                elif num == cube_num * 100 or is_axial_cube(desc):
                    res['cube_reformat'] = True
                    cube_num             = num / 100
                    self.review_list.append((series['/Path'], series['Description']))
                elif num >= cube_num * 100 + 1:
                    if is_coronal_reformat(desc):
                        res['cor_reformat'] = True
                        self.review_list.append((series['/Path'], series['Description']))
                    elif is_sagittal_reformat(desc):
                        res['sag_reformat'] = True
                        self.review_list.append((series['/Path'], series['Description']))
        self.valid_anatomic = res
        check               = [val for (key, val) in res.iteritems()]
        valid               = False if False in check else True
        return valid


class ProstateRaw(object):
    """Exam/Series information for a prostate exam. Contains methods to validate
    series data for processing."""
    def __init__(self, spectra_list=[], ideal_list=[]):
        self.spectra  = spectra_list
        self.ideal    = ideal_list
        self.full     = None
        self.w        = None
        self.sw       = None
        self.failures = {}
        self.sort_spectra()

    def sort_spectra(self):
        """Determines full, water, and suppressed spectra based on file size
        and order of aquisition."""
        water = []
        for s in self.spectra:
            if (os.path.getsize(s) >> 20) > 100:
                self.full = s
            elif (os.path.getsize(s) >> 20) < 1:
                water.append(s)
        water = sorted(water)
        if len(water) == 2:
            self.w, self.sw = water

    def validate(self):
        """Validates if all 3 spectra are present."""
        valid = True if self.full and self.w and self.sw and self.ideal else False
        if not valid:
            if not self.full:
                self.failures['full'] = False
            if not self.w:
                self.failures['w'] = False
            if not self.sw:
                self.failures['sw'] = False
            if not self.ideal:
                self.failures['ideal'] = False
        return valid


class ValidationFail(Exception):
    """Exception class to pass along validation failures from ProstateExam
    and ProstateRaw objects."""
    exam_msgs = {
                 'dce':      {
                              'dce_transferred': "DCE series is incomplete/missing.",
                              'pre_transferred': "preGd series is incomplete/missing."
                             },
                 'dwi':      {
                              'transferred': "bval=1350 DWI series is incomplete/missing.",
                              'corrected':   "Coil corrected b=1350 DWI seris is incomplete/missing"
                             },
                 'ideal':    {
                              'transferred': "Ideal series is incomplete/missing.",
                              'corrected':   "Coil corrected Ideal series is incomplete/missing."
                             },
                 'anatomic': {
                              't1_reformat':   "Coil corrected T1 series is incomplete/missing.",
                              't2_reformat':   "Coil corrected T2 series is incomplete/missing.",
                              'cube_reformat': "Coil corrected Cube series is incomplete/missing.",
                              'cor_reformat':  "Coronal reformat is incomplete/missing.",
                              'sag_reformat':  "Sagittal reformat is incomplete/missing."
                             },
                 'adc':     {
                             'rfov':           'rfov series is missing',
                             'rfov_brachy':    'rfov_brachy series is missing',
                             'high-b':         'high-b series is missing',
                             'high-b_cc':      'high-b_cc series is missing',
                             'high-b_rfov':    'high-b_rfov series is missing',
                             'high-b_rfov_cc': 'high-b_rfov_cc series is missing',
                             'std':            'std series is missing',
                             'std_brachy':     'std_brachy series is missing'
                            }
                }
    raw_msgs  = {
                 'w':     "Water spectra is missing.",
                 'sw':    "Water suppressed spectra is missing.",
                 'full':  "Full spectra is missing.",
                 'ideal': "Raw ideal is missing"
                }

    def __init__(self, exam_dict={}, raw_dict={}):
        self.exam_messages = []
        self.raw_messages  = []
        for base_key, failures in exam_dict.iteritems():
            for series, status in failures:
                message = ValidationFail.exam_msgs[base_key][series]
                self.exam_messages.append(message)
        for spectra, failure in raw_dict.iteritems():
            message = ValidationFail.raw_msgs[spectra]
            self.raw_messages.append(message)

    @dict_format
    def __str__(self):
        return {
                'Series Failures': sorted(['* ' + m for m in self.exam_messages]),
                'Raw Failures':    sorted(['* ' + m for m in self.raw_messages])
               }


class AuthenticationFail(Exception):
    pass
