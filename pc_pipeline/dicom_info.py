#!/netopt/bin/local/prostate/python27/bin/python2.7

#   $URL$
#   $Rev$
#   $Author$
#   $Date$
#


import os
import re
import fnmatch
from   datetime  import date
from   pprint    import pformat
from   multiprocessing.dummy import Pool as ThreadPool

import dicom    as dcm
import argparse as ap


# Walker #######################################################################
def listFiles(root, patterns='*', recurse=1, return_folders=0):
    # Expand patterns from semicolon-separated string to list
    pattern_list = patterns.split(';')
    # Collect input and output arguments into one bunch

    class Bunch:
        def __init__(self, **kwds):
            self.__dict__.update(kwds)

    arg = Bunch(recurse        = recurse,
                pattern_list   = pattern_list,
                return_folders = return_folders,
                results        = [])

    def visit(arg, dirname, files):
        # Append to arg.results all relevant files (and perhaps folders)
        for name in files:
            fullname = os.path.normpath(os.path.join(dirname, name))
            if arg.return_folders or os.path.isfile(fullname):
                for pattern in arg.pattern_list:
                    if fnmatch.fnmatch(name, pattern):
                        arg.results.append(fullname)
                        break
        # Block recursion if recursion was disallowed
        if not arg.recurse:
            files[:] = []

    os.path.walk(root, visit, arg)
    return arg.results


def dict_format(func):
    def formatted(self):
        pformat_string = pformat(func(self))
        return re.sub("[{}']", ' ', pformat_string).replace(',', '')
    return formatted


class ImageInfo(object):
    """docstring for HeaderInfo"""
    def __init__(self, image):
        super(ImageInfo, self).__init__()
        ds = dcm.read_file(image)
        self.ds   = ds
        self.exam = {
                     'Accession'   : self.get_ds_value('AccessionNumber'),
                     'Exam'        : self.get_ds_value('StudyID'),
                     'Description' : self.get_ds_value('StudyDescription').strip(),
                     'Modality'    : self.get_ds_value('Modality'),
                     'Protocol'    : self.get_ds_value('ProtocolName'),
                     'Patient'     : {
                                      'MRN'  : self.get_ds_value('PatientID'),
                                      'Name' : self.get_ds_value('PatientName')
                                     },
                     'Time'        : ':'.join([self.get_ds_value('StudyTime')[0:2],
                                               self.get_ds_value('StudyTime')[2:4],
                                               self.get_ds_value('StudyTime')[4:6]]),
                     'Station'     : {
                                      'Name'  : self.get_ds_value('StationName'),
                                      'Make'  : self.get_ds_value('Manufacturer'),
                                      'Model' : self.get_ds_value('ManufacturersModelName'),
                                      'Field' : self.get_ds_value('MagneticFieldStrength')
                                     }
                    }
        if self.exam['Station']['Field'] != 'Missing Tag':
            self.exam['Station']['Field'] = str(self.exam['Station']['Field']) + 'T'
        try:
            self.exam['Date'] = str(date(int(self.get_ds_value('StudyDate')[0:4]),
                                         int(self.get_ds_value('StudyDate')[4:6]),
                                         int(self.get_ds_value('StudyDate')[6:])))
        except Exception, e:
            self.exam['Date'] = 'Missing Tag'

        old_name = self.exam['Patient']['Name'].split('^')
        old_name = [c for c in old_name if c != '']
        self.exam['Patient']['Name'] = ", ".join(old_name)

        self.series = {'Description' : self.get_ds_value('SeriesDescription').strip()}
        try:
            self.series['Series'] = int(self.ds.SeriesNumber)
        except Exception, e:
            self.series['Series'] = 'Missing Tag'
        try:
            self.series['Resolution'] = ([float(s) for s in self.ds.PixelSpacing] + 
                                         [float(self.ds.SliceThickness)])
        except Exception, e:
            self.series['Resolution'] = 'Missing Tag'
        try:
            self.series['Matrix'] = [int(self.ds.Rows), 
                                     int(self.ds.Columns)]
        except Exception, e:
            self.series['Matrix'] = 'Missing Tag'
        try:
            self.series['Images'] = int(self.ds.ImagesInAcquisition)
        except Exception, e:
            self.series['Images'] = 'Missing Tag'

        if ds.SOPClassUID == '1.2.840.10008.5.1.4.1.1.4' or ds.Modality == 'MR':
            mr_header = {}
            try:
                mr_header.update({'PSD': ds['0019', '109c'].value})
            except Exception, e:
                mr_header.update({'PSD': 'Missing Tag'})
            try:
                mr_header.update({'TR': float(ds.RepetitionTime)})
            except Exception, e:
                mr_header.update({'TR': 'Missing Tag'})
            try:
                mr_header.update({'TE': float(ds.EchoTime)})
            except Exception, e:
                mr_header.update({'TE': 'Missing Tag'})
            try:
                mr_header.update({'TI': float(ds.InversionTime)})
            except Exception, e:
                mr_header.update({'TI': 'Missing Tag'})

            self.series.update(mr_header)

    def get_ds_value(self, tag_name):
        try:
            return self.ds.data_element(tag_name).value
        except Exception, e:
            return 'Missing Tag'

    @dict_format
    def __str__(self):
        return self.summarize()

    def dump(self):
        print self.ds.__str__()

    @dict_format
    def summarize_exam(self):
        return self.exam

    @dict_format
    def summarize_series(self):
        return [{s['Series']: s} for s in self.series]

    def summarize(self):
        info   = {
                  'Exam':   self.exam,
                  'Series': [{s['Series']: s} for s in self.series]
                 }
        return info


class ExamWalker(object):
    """docstring for ExamWalker"""
    def __init__(self, exam_dir):
        super(ExamWalker, self).__init__()
        self.directory   = exam_dir
        self.dicom_list  = []
        self.exam        = {}
        self.series      = []
        self.info        = {}
        self.find_dicoms()
        self.get_info()

    @dict_format
    def __str__(self):
        info   = {
                  'Exam':   self.exam,
                  'Series': [{s['Series']: s} for s in self.series]
                 }
        return info

    def find_dicoms(self):
        [self.dicom_list.append(d) for d in listFiles(os.path.join(self.directory),
         patterns='*.dcm;*.DCM')]
        self.dicom_list.sort()

        self.series_dict = {}
        self.series_dict = {os.path.split(dicom)[0]: os.path.split(dicom)[1]
                            for dicom in self.dicom_list
                            if os.path.split(dicom)[0] not in self.series_dict}

    def get_info(self):
        self.info   = {}
        self.exam   = {}
        self.series = []
        try:
            num_threads = len(self.series_dict) if len(self.series_dict) <= 10 else 10
            pool        = ThreadPool(num_threads)
        except Exception:
            self.info = None
            return None
        else:
            def info_worker(series_tup):
                series, dicom = series_tup
                try:
                    image = ImageInfo(os.path.join(series, dicom))
                except Exception:
                    pass
                else:
                    if not self.exam:
                        self.exam = image.exam
                    image.series.update({'/Path' : series})
                    self.series.append(image.series)
            pool.map(info_worker, self.series_dict.iteritems())
            pool.close()
            pool.join()
            self.series = sorted(self.series, key=lambda k:k['Series'])
            self.exam['/Path']  = self.directory
            self.info['Exam']   = self.exam
            self.info['Series'] = self.series
            return self.info


class CompleteExamWalker(ExamWalker):
    def __init__(self, directory):
        super(CompleteExamWalker, self).__init__(directory)
        self.transfer_filter()

    def transfer_filter(self):
        i = 0
        for series in self.series:
            try:
                num_images = int(series['Images'])
            except Exception:
                self.series[i]['Transferred'] = False
            else:
                num_files  = len(listFiles(series['/Path'], patterns='*.dcm;*.DCM'))
                if num_files == num_images:
                    self.series[i]['Transferred'] = True
                else:
                    self.series[i]['Transferred'] = False
            finally:
                i += 1


class MessyWalker(ExamWalker):
    """docstring for MessyWalker"""
    def __init__(self, directory):
        super(MessyWalker, self).__init__(directory)

    def find_dicoms(self):
        [self.dicom_list.append(d) for d in listFiles(os.path.join(self.directory),
         patterns='*.dcm;*.DCM')]
        self.dicom_list.sort()

        self.exam_dict = {}
        [self.load_dicoms(dicom) for dicom in self.dicom_list]

    def load_dicoms(self, dicom):
        image  = ImageInfo(dicom)
        exam   = image.exam
        series = image.series

        if exam['Exam'] not in self.exam_dict:
            self.exam_dict[exam['Exam']] = {'Exam': {}, 'Series': {}}
        self.exam_dict[exam['Exam']]['Exam'] = exam
        self.exam_dict[exam['Exam']]['Series'].update({series['Series']: series})

    def get_info(self):
        self.info = self.exam_dict
        return self.info


def main(args):
    if not args.dump:
        if os.path.isfile(args.dicom) and os.path.splitext(args.dicom)[1].lower() == '.dcm':
            try:
                dicom = ImageInfo(args.dicom)
                print dicom
            except Exception, e:
                raise e
        elif os.path.isdir(args.dicom):
            try:
                exam = CompleteExamWalker(args.dicom)
                print exam
            except Exception, e:
                raise e
        else:
            print "Please supply either a directory or .dcm/.DCM file."
    else:
        if os.path.isfile(args.dicom) and os.path.splitext(args.dicom)[1].lower() == '.dcm':
            try:
                dicom = ImageInfo(args.dicom)
                dicom.dump()
            except Exception, e:
                raise e
        else:
            print "Please supply a .dcm/.DCM file with the -d/--dump flag."


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """Summarize dicom header info for an exam, series or dicom file. By default,
    assumes series are separated into individual directories. Searches entire
    directory tree provided.""")
    parser.add_argument('-d', '--dump',
                        action = "store_true",
                        help = 'Dump complete dicom header for individual .dcm file')
    parser.add_argument('-m', '--messy',
                        action = "store_true",
                        help =
    """For directories where dicoms are not organized into series directories. Will
    parse all files for exam and series data, so will be slow.""")
    parser.add_argument('dicom', nargs='?', default=None,
                        help="Exam, series, or .dcm")
    return parser.parse_args()

if __name__ == '__main__':
    args = arg_set_up()
    main(args)
