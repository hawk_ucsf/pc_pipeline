#!/netopt/bin/local/prostate/python27/bin/python2.7

#   $URL$
#   $Rev$
#   $Author$
#   $Date$
#


import socket
import os
from   time import time

UCSF_root_UID = '1.3.6.1.4.1.20319.'

def unique_uid():
    """Creates a unique identifiers for dicom files

    Uses the machine ip address, user id, proccess id and time.
    UIDs in the root.1929 range are reserved for creation using the following
    method and require a ucsf ip address.
    
    UID = root  1929  ip . user_id  proc_id . time
            26   4    5  1   5        5     1  17
    
    """
    uid     = UCSF_root_UID
    ip      = socket.gethostbyname(socket.gethostname())
    ip      = [int(ii) for ii in ip.split('.')]
    ipnum   = ip[2]*256+ip[3]
    ipnum   = '1929%05d' % ipnum
    # assert len(ipnum) <= 9
    user_id = os.getuid()
    proc_id = os.getpid()
    up_id   = '%d%05d' % (user_id, proc_id)
    # assert len(up_id) <= 10
    time_id = repr(time())
    # assert len(time_id) <= 17

    uid += ipnum
    uid += '.'
    uid += up_id
    uid += '.'
    uid += time_id
    # assert len(uid) <= 64

    return uid

