#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
import os
import shutil as sh
import errno
import subprocess  as sub
import argparse    as ap
import logging     as lg
from   logging     import handlers
from   pc_data     import ProstateExam, ValidationFail
from   pc_get_data import ProstateSearcher
from   pc_utils    import change_permissions, gunzip_to_temp

logger           = lg.getLogger('pc_dcm_to_idf')
log_stdout       = lg.StreamHandler(sys.stdout)
file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stream_formatter = lg.Formatter('%(message)s')
log_stdout.setFormatter(stream_formatter)
log_stdout.setLevel(lg.INFO)
logger.addHandler(log_stdout)
logger.setLevel(lg.DEBUG)


def main(args):
    exam_path = os.path.abspath(args.exam)
    if not args.series:
        try:
            logger.info("Parsing exam...")
            pc_exam = ProstateExam(exam_path)
            logger.info("Validating exam...")
            pc_exam.validate()
        except ValidationFail as VF:
            logger.info('Validation failed:\n%s', VF)
        finally:
            series_list = pc_exam.import_list
            if not series_list:
                logger.info('Could not find any relevant series to convert.')
                return None
    else:
        series_list = [os.path.join(exam_path, str(series)) for series in args.series
                       if os.path.isdir(os.path.join(exam_path, str(series)))]
        if not series_list:
                logger.info('Could not find any of the series specified.')
                return None
    if series_list:
        logger.info("Starting conversions...")
        converted       = []
        exam_root, exam = os.path.split(exam_path)
        if not args.target:
            image_dir_path  = os.path.join(exam_root, 'images')
            try:
                os.makedirs(image_dir_path)
            except OSError, e:
                if e.errno == errno.EEXIST:
                    pass
                else:
                    logger.exception('Could not create %s, error:\n%s', image_dir_path, e)
                    return None
            except Exception, e:
                logger.exception('Could not create images directory, error:\n%s', e)
                return None
            else:
                change_permissions(image_dir_path)
        else:
            image_dir_path = os.path.abspath(args.target)
        name_base = os.path.join(image_dir_path, args.prefix)
        for path in series_list:
            series_num  = str(os.path.split(path)[1])
            series_name = exam + 'S' + series_num
            file_name   = '_'.join([name_base, series_name, 'div'])
            if os.path.isfile(file_name + '.idf') and not args.force:
                logger.info(file_name + '.idf already exists.\nUse --force to overwrite existing files.')
                return
            try:
                first_dicom = [d for d in sorted(os.listdir(path)) if '.dcm' in d.lower()][0]
            except Exception, e:
                logger.info('No dicoms found in %s.', path)
                continue
            if first_dicom[-3:].lower() == '.gz':
                logger.info('Compressed dicoms: unzipping into /tmp')
                try:
                    path = gunzip_to_temp(path)
                except Exception, e:
                    logger.exception('Error unzipping dicoms:\n%s', e)
                    return
                else:
                    first_dicom = [d for d in sorted(os.listdir(path))][0]
            first_dicom = os.path.join(path, first_dicom)
            command     = [ProstateSearcher.SVK_CONVERT,
                           '-i',
                           first_dicom,
                           '-o',
                           file_name,
                           '-t',
                           '3']
            try:
                logger.info('Converting %s...', path)
                logger.debug(command)
                res = sub.check_call(command)
            except sub.CalledProcessError, e:
                logger.info('Error converting %s to .idf', path)
                logger.debug(e)
            else:
                logger.info('%s converted to %s.idf', path, file_name)
                converted.append(path)
                change_permissions(file_name + '.idf')
                change_permissions(file_name + '.int2')
                if path[:4] == '/tmp':
                    sh.rmtree(path)
        return converted

def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Converts dicom images into .idf/.int2 files. Takes a prostate exam directory,
    performs a validation check, looking for coil-corrected anatomic and DWI files,
    and converts these series, placing the images into an 'images/' directory at
    the same level as the exam. A list of series numbers can be supplied, and the
    validation step will be ignored, converting all specified series. By default,
    files names with be E#####S#_div.idf. The prefix option will prepend a string,
    i.e. prefix_E#####S#_div.idf.
    """)
    requiredNamed = parser.add_argument_group("required named arguments")
    requiredNamed.add_argument('-e', '--exam',
                               required = True,
                               help     = "the prostate exam directory containing series to convert")
    parser.add_argument('-s', '--series',
                        dest  = "series",
                        type  = int,
                        nargs = "+",
                        help  = "a list of series numbers to convert")
    parser.add_argument('-p', '--prefix',
                        dest    = "prefix",
                        default = '',
                        help    = "string to prepend to filenames")
    parser.add_argument('-t', '--target',
                        dest = "target",
                        help = "directory to write .idf's to, instead of ./images")
    parser.add_argument('-f', '--force',
                        action  = "store_true",
                        default = False,
                        help    = "overwrite existing images with same name")
    return parser.parse_args()

if __name__ == '__main__':
    args     = arg_set_up()
    LOG_ROOT = '/data/pca2/logs'
    try:
        log_file = handlers.RotatingFileHandler(os.path.join(LOG_ROOT, "pc_dcm_to_idf.log"),
                                                maxBytes    = 1024*1024*10,
                                                backupCount = 2)
        log_file.setFormatter(file_formatter)
        log_file.setLevel(lg.DEBUG)
        logger.addHandler(log_file)
    except Exception, e:
        logger.exception('Problem loading log file %s:\n%s', LOG_ROOT, e)
    main(args)
