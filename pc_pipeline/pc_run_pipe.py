#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import re
import argparse    as ap
import logging     as lg
import subprocess  as sub
from   pc_utils    import ucsf_log_append


def add_to_pcdb(logger):
    try:
        cwd    = os.getcwd()
        pc_num = os.path.basename(cwd)
        if not re.match(r'^pc[0-9]*$', pc_num) or '/data/cpros' not in cwd:
            logger.info("We're not in a /data/cpros PC directory. Please run pcdb_import manually, if you'd like to import.")
            return
        exam_num = [e for e in os.listdir(cwd) if re.match(r'^E[0-9]*$', e)][0]
        if not exam_num:
            logger.info("Couldn't find exam directory. Won't run pcdb_import.")
            return
        command  = ['pcdb_import', exam_num, pc_num]
        sub.check_call(command)
    except Exception, e:
        logger.exception('Problem running pcdb_import')


def main():
    from pc_run_dce    import run_dce, get_shift_list
    from pc_run_maps   import run_maps
    from pc_run_adc    import run_adc
    from pc_run_mrsi   import run_mrsi
    from pc_run_review import run_review

    logger           = lg.getLogger('pc_run_pipe')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()
    try:
        local_log = lg.FileHandler('run_pipe.log')
        local_log.setFormatter(file_formatter)
        local_log.setLevel(lg.DEBUG)
        logger.addHandler(local_log)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        shift_list = []
        proceed    = True
        if args.center_points:
            if len(args.center_points) != 6:
                raise Exception('Please supply 6 center point values')
            shift_list, proceed = get_shift_list(args.center_points, logger)
        if proceed:
            add_to_pcdb(logger)
            try:
                try:
                    run_dce(shift_list=shift_list, pc_num=args.pc_num, pipe_logger=logger)
                except Exception, e:
                    logger.exception(e)
                run_adc(pc_num=args.pc_num, pipe_logger=logger)
                run_maps(pc_num=args.pc_num, pipe_logger=logger)
                run_review(pc_num=args.pc_num, pipe_logger=logger)
            except Exception, e:
                logger.exception(e)
            finally:
                run_mrsi(pc_num=args.pc_num)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Kicks off the main DCE workflow, when run in a pc#### directory, created by
    pc_get_data. Will run: pc_run_dce, pc_run_adc, pc_run_maps and pc_run_mrsi.
    Results are written to perfusion/, tensor/, t1/, cancer_maps/,
    ductal/, and spectra/. Auto-detects pc_number from directory.
    To run separately use: pc_run_dce.dev, pc_run_adc.dev, pc_run_maps.dev, or
    pc_run_mrsi.dev.
    """)
    parser.add_argument('-c', '--center_points',
                        dest  = "center_points",
                        type  = float,
                        nargs = "+",
                        help  = "6 values representing the T2 and DCE center points, in LPS order")
    parser.add_argument('--pc_num',
                        dest = "pc_num",
                        help = "optional pc_number to use in map naming, i.e. pc1234")
    return parser.parse_args()

if __name__ == '__main__':
    main()
