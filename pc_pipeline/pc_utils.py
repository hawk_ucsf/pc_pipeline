import sys
import os
import re
import errno
import gzip
import glob
import tempfile
import subprocess as sub


def mkdirs(newdir, mode=0775):
    try:
        os.makedirs(newdir, mode)
    except OSError, err:
        # Reraise the error unless it's about an already existing directory
        if err.errno != errno.EEXIST or not os.path.isdir(newdir):
            raise


def ucsf_log_append(started=True):
    Logfile_string = ' '.join(sys.argv)
    try:
        if started:
            sub.call(["log_processing", "-l", ".", "-s", Logfile_string])
        else:
            sub.call(["log_processing", "-l", ".", "-e", Logfile_string])
    except Exception:
        pass


def change_permissions(directory):
    """Recursively change group to 'prostate' and set permissions to
    770 on a directory tree."""
    try:
        sub.call(["chgrp", "-R", "prostate", directory])
        sub.call(["chmod", "-R", "770", directory])
        sub.call(["chmod", "-R", "g+s", directory])
    except Exception, e:
        print e
    else:
        return True


def natural_key(string_):
    """See http://www.codinghorror.com/blog/archives/001018.html"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]


def gunzip_to_temp(old_path):
    tmp_dir = tempfile.mkdtemp()
    for zipped in glob.glob(os.path.join(old_path, '*.gz')):
        base      = os.path.basename(zipped)
        dest_name = os.path.join(tmp_dir, base[:-3])
        with gzip.open(zipped, 'rb') as infile:
            with open(dest_name, 'w') as outfile:
                for line in infile:
                    outfile.write(line)
    return tmp_dir


def sym_link(src, dst, link_name=None):
    basename = link_name if link_name else os.path.basename(src)
    try:
        os.symlink(src, os.path.join(dst, basename))
    except OSError, err:
        if err.errno != errno.EEXIST:
            raise err
