#!/netopt/bin/local/prostate/python27/bin/python2.7

import sys
for p in sys.path:
    if 'henrylab' in p:
        sys.path.remove(p)
import os
import shutil      as sh
import argparse    as ap
import subprocess  as sub
import fnmatch
import dicom       as dcm
from   collections import namedtuple
from   pc_pipe     import ProstatePipeline
from   pc_utils    import mkdirs, change_permissions, ucsf_log_append, sym_link
from   pc_data     import ProstateExam
import logging     as lg


class ProstatePACSValidator(ProstateExam):
    """
    A simple validator, based off of pc_get_data's ProstateExam. Looks for
    pipeline and Sivic results. Doesn't raise exception, just collects whatever
    results have been generated.
    """

    def __init__(self, exam_directory):
        super(ProstatePACSValidator, self).__init__(exam_directory)

    def get_relevant_series(self):
        """Pares down relevant series builder to just DCE series."""
        self.reid_series   = self.series[0]['/Path']
        self.combo_dce     = None
        self.combo_pregd   = None
        self.perfusion_wo  = None
        self.perfusion_pk  = None
        self.perfusion_sl  = None
        self.perfusion_wo  = None
        self.tensor_adc    = None
        self.tensor_avg    = None
        self.sivic_mrs     = None
        self.sivic_dce_l   = None
        self.sivic_dce_r   = None
        self.sivic_dce_all = None
        self.axials        = []
        for idx, series in enumerate(self.series):
            if self.is_combo(series):
                if '_t' not in os.path.basename(series['/Path']):
                    if fnmatch.fnmatch(series['Description'].lower(), 'dce*'):
                        self.combo_dce = series['/Path']
                    elif fnmatch.fnmatch(series['Description'].lower(), 'pregd*'):
                        self.combo_pregd = series['/Path']
            elif self.is_perfusion(series):
                if fnmatch.fnmatch(series['/Path'], '*pc*_peak_ht'):
                    self.perfusion_pk = series['/Path']
                elif fnmatch.fnmatch(series['/Path'], '*pc*_slope'):
                    self.perfusion_sl = series['/Path']
                elif fnmatch.fnmatch(series['/Path'], '*pc*_washout_pos'):
                    self.perfusion_wo = series['/Path']
            elif self.is_tensor(series):
                if fnmatch.fnmatch(series['/Path'], '*pc*_adc'):
                    self.tensor_adc = series['/Path']
                elif fnmatch.fnmatch(series['/Path'], '*pc*_avg'):
                    self.tensor_avg = series['/Path']
            elif self.is_sivic(series):
                if fnmatch.fnmatch(series['/Path'], '*SIVIC_SC_MRS'):
                    self.sivic_mrs     = series['/Path']
                elif fnmatch.fnmatch(series['/Path'], '*SIVIC_SC_DCE_L'):
                    self.sivic_dce_l   = series['/Path']
                elif fnmatch.fnmatch(series['/Path'], '*SIVIC_SC_DCE_R'):
                    self.sivic_dce_r   = series['/Path']
                elif fnmatch.fnmatch(series['/Path'], '*SIVIC_SC_DCE_ALL'):
                    self.sivic_dce_all = series['/Path']

        review_images = 'review_images'
        if os.path.isdir(review_images):
            review = [os.path.join(review_images, d) for d in os.listdir(review_images)]
            for image in review:
                if self.is_axial(image):
                    self.axials.append(image)

    def validate(self):
        return True

    def is_combo(self, series):
        if fnmatch.fnmatch(series['/Path'], 'DCE_combined_DICOM/*'):
            return True

    def is_perfusion(self, series):
        if fnmatch.fnmatch(series['/Path'], 'perfusion/dcm_output/*'):
            return True

    def is_sivic(self, series):
        if fnmatch.fnmatch(series['/Path'], 'SIVIC_DICOM_SC/*'):
            return True

    def is_tensor(self, series):
        if fnmatch.fnmatch(series['/Path'], 'tensor/*'):
            return True

    def is_axial(self, path):
        if fnmatch.fnmatch(path, 'review_images/pc*_Ax_*T1'):
            return True
        elif fnmatch.fnmatch(path, 'review_images/pc*_Ax_*T2'):
            return True


class ProstatePACSPipeline(ProstatePipeline):
    """
    Reidentifies, window-levels, and sends to PACS/Dynacad:
        - Sivic Screenshots
            - Spectra
            - DCE L/R/ALL
        - DWI Maps
            - ADC
            - High-b Average
        - DCE Combined Coils
        - Perfusion Maps
            - Peak Ht
            - Up Slope
            - Washout
    """
    pacs_export_dir    = "/data/quipc/export/prostate/PACS"
    dynacad_export_dir = "/data/quipc/export/prostate/Dynacad-cli"

    def __init__(self, shift_list=None, pc_num=None, pipe_logger=None):
        super(ProstatePACSPipeline, self).__init__(pc_num=pc_num, pipe_logger=pipe_logger)

    def set_test(self):
        export_dir = os.path.join(self.pc_directory, 'export_test')
        self.pacs_export_dir    = os.path.join(export_dir, 'pacs')
        self.dynacad_export_dir = os.path.join(export_dir, 'dynacad')
        mkdirs(self.pacs_export_dir)
        mkdirs(self.dynacad_export_dir)
        change_permissions(self.pacs_export_dir)
        change_permissions(self.dynacad_export_dir)

    def set_validator(self):
        self.validator = ProstatePACSValidator

    def get_exams(self):
        self.exam_paths = ['.']

    def set_up_directories(self):
        pacs_export  = os.path.join(self.pc_directory, 'PACS_Export')
        spectra      = os.path.join(pacs_export, 'spectra')
        dce_l        = os.path.join(pacs_export, 'dce_l')
        dce_r        = os.path.join(pacs_export, 'dce_r')
        dce_all      = os.path.join(pacs_export, 'dce_all')
        adc          = os.path.join(pacs_export, 'b=600_ADC')
        avg          = os.path.join(pacs_export, 'high_b_combined')
        combo        = os.path.join(pacs_export, 'DCE_combined')
        perfusion_pk = os.path.join(pacs_export, 'perfusion_maps_dce_pv')
        perfusion_sl = os.path.join(pacs_export, 'perfusion_maps_dce_init_slope')
        perfusion_wo = os.path.join(pacs_export, 'perfusion_maps_washout_slope')
        mkdirs(spectra)
        mkdirs(dce_l)
        mkdirs(dce_r)
        mkdirs(dce_all)
        mkdirs(adc)
        mkdirs(avg)
        mkdirs(combo)
        mkdirs(perfusion_pk)
        mkdirs(perfusion_sl)
        mkdirs(perfusion_wo)
        change_permissions(pacs_export)
        change_permissions(spectra)
        change_permissions(dce_l)
        change_permissions(dce_r)
        change_permissions(dce_all)
        change_permissions(adc)
        change_permissions(avg)
        change_permissions(combo)
        change_permissions(perfusion_pk)
        change_permissions(perfusion_sl)
        change_permissions(perfusion_wo)
        Directories = namedtuple('Directories', ['pacs_export',
                                                 'spectra',
                                                 'dce_l',
                                                 'dce_r',
                                                 'dce_all',
                                                 'adc',
                                                 'avg',
                                                 'combo',
                                                 'perfusion_pk',
                                                 'perfusion_sl',
                                                 'perfusion_wo'])
        self.directories = Directories(pacs_export,
                                       spectra,
                                       dce_l,
                                       dce_r,
                                       dce_all,
                                       adc,
                                       avg,
                                       combo,
                                       perfusion_pk,
                                       perfusion_sl,
                                       perfusion_wo)
        return self.directories

    def _export_dcm_drop(self, series, destination):
        dicom_list = [os.path.join(series, d) for d in os.listdir(series)
                      if '.dcm' in d.lower()]
        if dicom_list:
            for dicom in dicom_list:
                sh.copy(dicom, destination)
        else:
            raise Exception('No dicoms found in series.')

    def _export_apps(self, series, destination):
        dicom_list = [os.path.join(series, d) for d in os.listdir(series)
                      if '.dcm' in d.lower()]
        if dicom_list:
            link_name = '_'.join([self.pc_num, os.path.basename(series)])
            link_src  = os.path.abspath(series)
            sym_link(link_src, destination, link_name = link_name)
            apps_link = os.path.join(destination, link_name)
            sub.call(["chgrp", "quipc", apps_link])
            sub.call(["chmod", "775", apps_link])
        else:
            raise Exception('No dicoms found in series.')

    def export_to_pacs(self, series):
        self.logger.info('Exporting %s to PACS', series)
        try:
            self._export_apps(series, self.pacs_export_dir)
        except Exception:
            self.logger.exception('Error exporting %s to PACS', series)
        else:
            self.logger.info('Success exporting %s to PACS', series)

    def export_to_dynacad(self, series):
        self.logger.info('Exporting %s to Dynacad', series)
        try:
            self._export_apps(series, self.dynacad_export_dir)
        except Exception:
            self.logger.exception('Error exporting %s to Dynacad', series)
        else:
            self.logger.info('Success exporting %s to Dynacad', series)

    def set_window_level(self, series):
        self.logger.info('Setting W/L for %s', series)
        dicom_list = [os.path.join(series, d) for d in os.listdir(series)
                      if '.dcm' in d.lower()]
        try:
            command = ['svk_file_convert.dev',
                       '-i', dicom_list[0],
                       '--info']
            output  = sub.check_output(command)
            window  = None
            level   = None
            for line in output.split('\n'):
                if 'WINDOW' in line:
                    window = line.split()[-1]
                elif 'LEVEL' in line:
                    level = line.split()[-1]
            if not window or not level:
                raise Exception('No window or level found')
            for dicom in dicom_list:
                ds              = dcm.read_file(dicom)
                ds.WindowCenter = float(window)
                ds.WindowWidth  = float(level)
                ds.save_as(dicom)
        except Exception:
            self.logger.exception('Error setting W/L for %s', series)
        else:
            self.logger.info('Success setting W/L for %s', series)

    def reidentify_series(self, series):
        command = ['/netopt/share/bin/local/brain/reidentify_images',
                   '--in_dir', series,
                   '--id_images', self.exam_validators[0].reid_series,
                   '--nb']
        try:
            sub.check_call(command)
        except Exception, e:
            self.logger.exception('Error setting ids for %s', series)
            raise e

    def run_series(self, series, export_dir, create=True, window_level=True, pacs=True, dynacad=True):
        if create:
            if os.listdir(export_dir):
                raise Exception('Dicoms already generated in %s' % export_dir)
            self.copy_series_to_dir(series, export_dir)
            self.reidentify_series(export_dir)
            if window_level:
                self.set_window_level(export_dir)
        if pacs:
            self.export_to_pacs(export_dir)
        if dynacad:
            self.export_to_dynacad(export_dir)

    def run_combo(self, create=True, pacs=True, dynacad=True):
        self.logger.info('Preparing to export combo coil')
        validator = self.exam_validators[0]
        if validator.combo_dce:
            series_num  = os.path.basename(validator.combo_dce)
            export_dest = os.path.join(self.directories.combo, series_num)
            mkdirs(export_dest)
            change_permissions(export_dest)
            self.run_series(validator.combo_dce,
                            export_dest,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)
        if validator.combo_pregd:
            series_num  = os.path.basename(validator.combo_pregd)
            export_dest = os.path.join(self.directories.combo, series_num)
            mkdirs(export_dest)
            change_permissions(export_dest)
            self.run_series(validator.combo_pregd,
                            export_dest,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)

    def run_perfusion(self, create=True, pacs=True, dynacad=True):
        self.logger.info('Preparing to export perfusion')
        validator = self.exam_validators[0]
        if validator.perfusion_pk:
            self.run_series(validator.perfusion_pk,
                            self.directories.perfusion_pk,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)
        if validator.perfusion_sl:
            self.run_series(validator.perfusion_sl,
                            self.directories.perfusion_sl,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)
        if validator.perfusion_wo:
            self.run_series(validator.perfusion_wo,
                            self.directories.perfusion_wo,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)

    def run_tensor(self, create=True, pacs=True, dynacad=True):
        self.logger.info('Preparing to export tensor')
        validator = self.exam_validators[0]
        if validator.tensor_adc:
            self.run_series(validator.tensor_adc,
                            self.directories.adc,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)
        if validator.tensor_avg:
            self.run_series(validator.tensor_avg,
                            self.directories.avg,
                            create  = create,
                            pacs    = pacs,
                            dynacad = dynacad)

    def run_sivic(self, mrs=True, dce_l=True, dce_r=True, dce_all=True, create=True, pacs=True):
        self.logger.info('Preparing to export screenshots')
        validator = self.exam_validators[0]
        if validator.sivic_mrs and mrs:
            self.run_series(validator.sivic_mrs,
                            self.directories.spectra,
                            create       = create,
                            window_level = False,
                            pacs         = pacs,
                            dynacad      = False)
        if validator.sivic_dce_l and dce_l:
            self.run_series(validator.sivic_dce_l,
                            self.directories.dce_l,
                            create       = create,
                            window_level = False,
                            pacs         = pacs,
                            dynacad      = False)
        if validator.sivic_dce_r and dce_r:
            self.run_series(validator.sivic_dce_r,
                            self.directories.dce_r,
                            create       = create,
                            window_level = False,
                            pacs         = pacs,
                            dynacad      = False)
        if validator.sivic_dce_all and dce_all:
            self.run_series(validator.sivic_dce_all,
                            self.directories.dce_all,
                            create       = create,
                            window_level = False,
                            pacs         = pacs,
                            dynacad      = False)

    def run_axials(self):
        self.logger.info('Preparing to export axials')
        validator = self.exam_validators[0]
        for axial in validator.axials:
            self.export_to_dynacad(axial)

    def run(self):
        self.logger.info('Exporting results')
        self.set_up_directories()
        self.run_combo()
        self.run_perfusion()
        self.run_tensor()
        self.run_sivic()
        self.run_axials()


def send_to_pacs(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run()


def no_send(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_combo(pacs=False, dynacad=False)
    test.run_perfusion(pacs=False, dynacad=False)
    test.run_tensor(pacs=False, dynacad=False)
    test.run_sivic(pacs=False)


def re_send(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_combo(create=False)
    test.run_perfusion(create=False)
    test.run_tensor(create=False)
    test.run_sivic(create=False)
    test.run_axials()


def gen_combo(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_combo(pacs=False, dynacad=False)


def gen_perfusion(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_perfusion(pacs=False, dynacad=False)


def gen_diffusion(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_tensor(pacs=False, dynacad=False)


def gen_sivic(mrs=True, dce_l=True, dce_r=True, dce_all=True, pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_sivic(mrs=mrs, dce_l=dce_l, dce_r=dce_r, dce_all=dce_all, pacs=False)


def send_combo(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_combo(create=False)


def send_perfusion(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_perfusion(create=False)


def send_diffusion(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_tensor(create=False)


def send_sivic(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.run_sivic(create=False)


def send_axial(pc_num=None, pipe_logger=None, test_export=False):
    test = ProstatePACSPipeline(pc_num=pc_num, pipe_logger=pipe_logger)
    if test_export:
        test.set_test()
    test.validate()
    test.set_up_directories()
    test.axials()


def main():
    logger           = lg.getLogger('send_to_pacs')
    log_stdout       = lg.StreamHandler(sys.stdout)
    file_formatter   = lg.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    stream_formatter = lg.Formatter('%(message)s')
    log_stdout.setFormatter(stream_formatter)
    log_stdout.setLevel(lg.INFO)
    logger.addHandler(log_stdout)
    logger.setLevel(lg.DEBUG)

    args = arg_set_up()

    try:
        local_log = lg.FileHandler('send_to_pacs.log')
        local_log.setFormatter(file_formatter)
        local_log.setLevel(lg.DEBUG)
        logger.addHandler(local_log)
        ucsf_log_append()
    except Exception, e:
        logger.exception('Problem loading log file')

    try:
        if args.no_send:
            no_send(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.re_send:
            re_send(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.gen_combo:
            gen_combo(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.gen_perfusion:
            gen_perfusion(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.gen_diffusion:
            gen_diffusion(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.gen_sivic:
            gen_sivic(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.gen_dce_l:
            gen_sivic(mrs         = False,
                      dce_l       = True,
                      dce_r       = False,
                      dce_all     = False,
                      pc_num      = args.pc_num,
                      pipe_logger = logger,
                      test_export = args.test)
        elif args.gen_dce_r:
            gen_sivic(mrs         = False,
                      dce_l       = False,
                      dce_r       = True,
                      dce_all     = False,
                      pc_num      = args.pc_num,
                      pipe_logger = logger,
                      test_export = args.test)
        elif args.gen_dce_all:
            gen_sivic(mrs         = False,
                      dce_l       = False,
                      dce_r       = False,
                      dce_all     = True,
                      pc_num      = args.pc_num,
                      pipe_logger = logger,
                      test_export = args.test)
        elif args.gen_spectra:
            gen_sivic(mrs         = True,
                      dce_l       = False,
                      dce_r       = False,
                      dce_all     = False,
                      pc_num      = args.pc_num,
                      pipe_logger = logger,
                      test_export = args.test)
        elif args.send_combo:
            send_combo(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.send_perfusion:
            send_perfusion(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.send_diffusion:
            send_diffusion(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.send_sivic:
            send_sivic(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        elif args.send_axial:
            send_axial(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
        else:
            send_to_pacs(pc_num=args.pc_num, pipe_logger=logger, test_export=args.test)
    except Exception, e:
        logger.exception(e)
    finally:
        ucsf_log_append(started=False)


def arg_set_up():
    # Set up argument parsing
    parser = ap.ArgumentParser(description =
    """
    Prepares results for export to PACS and Dynacad, and sends. See indivudual
    options, below:
    """)
    parser.add_argument('--pc_num',
                        dest    = "pc_num",
                        help    = "If not in a pc directory, enter the pc number.")
    parser.add_argument('--no_send',
                        dest    = "no_send",
                        action  = "store_true",
                        default = False,
                        help    = "Generate export results in PACS_Export/, but don't send")
    parser.add_argument('--re_send',
                        dest    = "re_send",
                        action  = "store_true",
                        default = False,
                        help    = "Re-send dicoms in PACS_Export/, don't generate results")
    parser.add_argument('--gen_combo',
                        dest    = "gen_combo",
                        action  = "store_true",
                        default = False,
                        help    = "Generate combo coil dicoms for export")
    parser.add_argument('--gen_perfusion',
                        dest    = "gen_perfusion",
                        action  = "store_true",
                        default = False,
                        help    = "Generate perfusion dicoms for export")
    parser.add_argument('--gen_diffusion',
                        dest    = "gen_diffusion",
                        action  = "store_true",
                        default = False,
                        help    = "Generate diffusion dicoms for export")
    parser.add_argument('--gen_sivic',
                        dest    = "gen_sivic",
                        action  = "store_true",
                        default = False,
                        help    = "Generate all sivic screenshot dicoms for export")
    parser.add_argument('--gen_dce_l',
                        dest    = "gen_dce_l",
                        action  = "store_true",
                        default = False,
                        help    = "Generate DCE L screenshot dicoms for export")
    parser.add_argument('--gen_dce_r',
                        dest    = "gen_dce_r",
                        action  = "store_true",
                        default = False,
                        help    = "Generate DCE R screenshot dicoms for export")
    parser.add_argument('--gen_dce_all',
                        dest    = "gen_dce_all",
                        action  = "store_true",
                        default = False,
                        help    = "Generate DCE ALL screenshot dicoms for export")
    parser.add_argument('--gen_spectra',
                        dest    = "gen_spectra",
                        action  = "store_true",
                        default = False,
                        help    = "Generate Spectra screenshot dicoms for export")
    parser.add_argument('--send_combo',
                        dest    = "send_combo",
                        action  = "store_true",
                        default = False,
                        help    = "Export combo coil dicoms")
    parser.add_argument('--send_perfusion',
                        dest    = "send_perfusion",
                        action  = "store_true",
                        default = False,
                        help    = "Export perfusion dicoms")
    parser.add_argument('--send_diffusion',
                        dest    = "send_diffusion",
                        action  = "store_true",
                        default = False,
                        help    = "Export diffusion dicoms")
    parser.add_argument('--send_sivic',
                        dest    = "send_sivic",
                        action  = "store_true",
                        default = False,
                        help    = "Export sivic screenshot dicoms")
    parser.add_argument('--send_axial',
                        dest    = "send_axial",
                        action  = "store_true",
                        default = False,
                        help    = "Export axial dicoms to dynacad")
    parser.add_argument('--test',
                        dest    = "test",
                        action  = "store_true",
                        default = False,
                        help    = "Exports to local 'test' directory")
    return parser.parse_args()

if __name__ == '__main__':
    main()
