import sys
import os
import re
import shutil   as sh
import logging  as lg
import dicom    as dcm
from   pc_utils import natural_key
from   pc_data  import ValidationFail


class ProstatePipeline(object):
    """
    Base class for creating Prostate workflows. Requires a validator to be set,
    and a run() method to execute the workflow. Sets a logger, and finds exams
    in the current directory for the validator to parse. Override validate()
    for custom validation.
    """

    def __init__(self, pc_num=None, pipe_logger=None):
        super(ProstatePipeline, self).__init__()
        if not pipe_logger:
            self.create_logger()
        else:
            self.logger = pipe_logger
        cwd               = os.getcwd()
        self.pc_num       = pc_num if pc_num else os.path.basename(cwd)
        self.pc_directory = os.path.abspath(cwd)
        if not re.match(r'^pc[0-9]*$', self.pc_num):
            raise Exception("We're not in a pc_num directory. Please supply a pc_num pc1234")
        self.exam_validators = []
        self.valid           = True
        self.set_validator()
        self.get_exams()

    def create_logger(self):
        self.logger      = lg.getLogger('pc_pipe')
        log_stdout       = lg.StreamHandler(sys.stdout)
        stream_formatter = lg.Formatter('%(message)s')
        log_stdout.setFormatter(stream_formatter)
        log_stdout.setLevel(lg.INFO)
        self.logger.addHandler(log_stdout)
        self.logger.setLevel(lg.DEBUG)

    def set_validator(self):
        """
        Set a validator, of based off of pc_get_data's ProstateExam.
        """
        raise NotImplementedError

    def get_exams(self):
        directories     = [d for d in os.listdir(os.getcwd()) if os.path.isdir(d)]
        self.exam_paths = [e for e in directories if re.match(r'^E[0-9]*$', e)]

    def _get_image_series_input(self, series):
        """
        Helper method to get image file for svk_* command line tools. If given a
        series directory, pulls out first dicom image. If given individual .idf
        or dicom, returns that.
        """
        if os.path.isdir(series):
            series_list = os.listdir(series)
            dicom_list  = [d for d in series_list if '.dcm' in d.lower()]
            dicom_list  = sorted(dicom_list, key=natural_key)
            first_dcm   = os.path.join(series, dicom_list[0])
            ds          = dcm.read_file(first_dcm)
            if 'ref' in ds.SeriesDescription.lower() and len(str(ds.SeriesNumber)) > 2:
                if len(dicom_list) > 1:
                    first_dcm = os.path.join(series, dicom_list[1])
        else:
            first_dcm = series
        return first_dcm

    def validate(self):
        """
        Iterates over exams in current directory, and passes them through the
        validator. Raises pc_get_data's ValidationFail exception on failure,
        logging the individual faults.
        """
        self.logger.info('Validating...')
        exam_fails = {}
        for exam in self.exam_paths:
            validator = self.validator(exam)
            valid     = validator.validate()
            self.exam_validators.append(validator)
            if not valid:
                self.valid = valid
            else:
                if not self.valid:
                    self.valid = valid
                break
        if not self.valid:
            for exam in self.exam_validators:
                if not exam_fails:
                    exam_fails.update(exam.failures)
                else:
                    # Compare with previous exam's failures
                    if not exam.failures:
                        exam.failures = {}
                    for key, val in exam.failures.iteritems():
                        if key in exam_fails:
                            # Get the overlap of exam failures
                            fails = [d for d in val if d in exam_fails[key]]
                            if not fails:
                                del exam_fails[key]
                            else:
                                exam_fails[key] = fails
                    keys_to_remove = []
                    for key, val in exam_fails.iteritems():
                        if key not in exam.failures:
                            keys_to_remove.append(key)
                    for key in keys_to_remove:
                        del exam_fails[key]
            raise ValidationFail(exam_dict=exam_fails)

    @staticmethod
    def set_series_description(dicom, description):
        def set_desc(d):
            ds = dcm.read_file(d)
            ds.SeriesDescription = description
            ds.save_as(d)
        if os.path.isdir(dicom):
            dicom_list = [os.path.join(dicom, f) for f in os.listdir(dicom) if '.dcm' in f.lower()]
            [set_desc(d) for d in dicom_list]
        else:
            set_desc(dicom)

    @staticmethod
    def set_series_number(dicom, number):
        def set_num(d):
            ds = dcm.read_file(d)
            ds.SeriesNumber = int(number)
            ds.save_as(d)
        if os.path.isdir(dicom):
            dicom_list = [os.path.join(dicom, f) for f in os.listdir(dicom) if '.dcm' in f.lower()]
            [set_num(d) for d in dicom_list]
        else:
            set_num(dicom)

    @staticmethod
    def copy_series_to_dir(series, destination):
        dicom_list = [os.path.join(series, d) for d in os.listdir(series)
                      if '.dcm' in d.lower()]
        for dicom in dicom_list:
            sh.copy(dicom, destination)

    def set_up_directories(self):
        raise NotImplementedError

    def run(self):
        raise NotImplementedError
