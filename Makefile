#
#   $URL$
#   $Rev$
#   $Author$
#   $Date$
#

#
#	this contains system wide variable defs and must be at the start of all Makefiles
#
-include /netopt/share/include/include.mk

PC_BIN_DIR = $(SCRIPTDIR)/prostate
PKG        = pc_get_data_pkg
PKG_DIR    = $(PC_BIN_DIR)/$(PKG)

pc_scripts = \
		dicom_info.py     \
		uid.py            \
		pc_data.py        \
		pc_pipe.py        \
		pc_get_data.py    \
		pc_combine_dce.py \
		pc_dcm_to_idf.py  \
		pc_utils.py       \
		pc_run_pipe.py    \
		pc_run_dce.py     \
		pc_run_maps.py    \
		pc_run_adc.py     \
		pc_run_mrsi.py    \
		pc_run_review.py  \
		pc_send_to_pacs.py

pc_usage_docs = \
		pc_get_data    \
		pc_combine_dce \
		pc_dcm_to_idf  \
		pc_run_pipe    \
		pc_run_dce     \
		pc_run_maps    \
		pc_run_adc     \
		pc_run_mrsi    \
		pc_run_review  \
		pc_send_to_pacs

#deploy: docs
deploy: 
	rrc_deploy.pl -a -s -d install 

deploy_dev:
	rrc_deploy.pl -a -s -t -d install_dev 

install: 
	+@[ -d $(PKG_DIR)/ ] || mkdir -p $(PKG_DIR)/
	${RM} -r $(PKG_DIR)/*
	cd pc_pipeline; \
	for file in $(pc_scripts); \
		do $(INSTALL) -m 775 $${file} $(PKG_DIR)/$${file}; \
	done
	ln -sf $(PKG_DIR)/pc_get_data.py $(PC_BIN_DIR)/pc_get_data;
	ln -sf $(PKG_DIR)/pc_combine_dce.py $(PC_BIN_DIR)/pc_combine_dce;
	ln -sf $(PKG_DIR)/pc_run_pipe.py $(PC_BIN_DIR)/pc_run_pipe;
	ln -sf $(PKG_DIR)/pc_run_dce.py $(PC_BIN_DIR)/pc_run_dce;
	ln -sf $(PKG_DIR)/pc_run_maps.py $(PC_BIN_DIR)/pc_run_maps;
	ln -sf $(PKG_DIR)/pc_run_adc.py $(PC_BIN_DIR)/pc_run_adc;
	ln -sf $(PKG_DIR)/pc_run_mrsi.py $(PC_BIN_DIR)/pc_run_mrsi;
	ln -sf $(PKG_DIR)/pc_run_review.py $(PC_BIN_DIR)/pc_run_review;
	ln -sf $(PKG_DIR)/pc_send_to_pacs.py $(PC_BIN_DIR)/pc_send_to_pacs;

install_dev: 
	+@[ -d $(PKG_DIR)_dev/ ] || mkdir -p $(PKG_DIR)_dev/
	${RM} -r $(PKG_DIR)_dev/*
	cd pc_pipeline; \
	for file in $(pc_scripts); \
		do $(INSTALL) -m 775 $${file} $(PKG_DIR)_dev/$${file}; \
	done
	ln -sf $(PKG_DIR)_dev/pc_get_data.py $(PC_BIN_DIR)/pc_get_data.dev; 
	ln -sf $(PKG_DIR)_dev/pc_combine_dce.py $(PC_BIN_DIR)/pc_combine_dce.dev; 
	ln -sf $(PKG_DIR)_dev/pc_run_pipe.py $(PC_BIN_DIR)/pc_run_pipe.dev;
	ln -sf $(PKG_DIR)_dev/pc_run_dce.py $(PC_BIN_DIR)/pc_run_dce.dev;
	ln -sf $(PKG_DIR)_dev/pc_run_maps.py $(PC_BIN_DIR)/pc_run_maps.dev;
	ln -sf $(PKG_DIR)_dev/pc_run_adc.py $(PC_BIN_DIR)/pc_run_adc.dev;
	ln -sf $(PKG_DIR)_dev/pc_run_mrsi.py $(PC_BIN_DIR)/pc_run_mrsi.dev;
	ln -sf $(PKG_DIR)_dev/pc_run_review.py $(PC_BIN_DIR)/pc_run_review.dev;
	ln -sf $(PKG_DIR)_dev/pc_send_to_pacs.py $(PC_BIN_DIR)/pc_send_to_pacs.dev;

docs:
	for script in $(pc_usage_docs); \
		do ./$${script}.py -h >> $${script}.txt; \
	done


clean: 
	$(RM) *.txt
	$(RM) *.dev 
	$(RM) *.tmp 

-include /netopt/share/include/deps.mk
