MRI/MRS Prostate Exam Analysis Utilities
========================================

Toolkit and pipeline controller for processing prostate exam data.
Performs DCE, Diffusion, and MRSI analysis. Sends results to PACS and
Dynacad.

Installation/Deployment
-----------------------

Note: depends on ``sivic`` installation.

RRCS Deployment
~~~~~~~~~~~~~~~

To deploy across radiology research systems (``pile-mb``, ``pile-cb``,
and ``pile-parn``), and have executables in ``prostate`` group's
``$PATH``:

::

    git clone https://<USERNAME>@bitbucket.org/quipcbitsadmin/pc_pipeline.git
    cd pc_pipeline
    make deploy

To deploy ``.dev`` development version:

::

    make deploy_dev

Sandboxed VM/Python Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::

    git clone https://<USERNAME>@bitbucket.org/quipcbitsadmin/pc_pipeline.git
    cd pc_pipeline
    python setup.py install

Workflow/Usage
==============

For information on running the pipeline, see ``USAGE.MD``, or the
project
`wiki <https://bitbucket.org/quipcbitsadmin/pc_pipeline/wiki/Home>`__ on
Bitbucket.
